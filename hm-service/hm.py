import json

from util import local
from utility import hm_utils
from util import ssac
from ams_api.kr.StairwAIDriver import Driver

class HorizontalMatchmaking():
    """
    Horizontal matchmaking service.
    """
    def __init__(self, verbose=0):
        if verbose > 0:
            print('>>> Initializing HM')

        # Init the driver
        if verbose > 0:
            print('>>> Initializing the AMS driver')
        self.driver = Driver()
        if verbose > 0:
            print('<<< Driver initilized')

        if verbose > 0:
            print('>>> Reading the horizontal matchmaking configuration file')
        # Read configuration parameters
        self.params = None
        with open('conf.json') as fp:
            self.params = json.load(fp)
        if verbose > 0:
            print('<<< Reading the horizontal matchmaking configuration file')

        if verbose > 0:
            print('>>> Loading labels')
        # Load labels
        # label_fname = os.path.normpath(params['label_fnme'])
        # labels = local.load_labels(label_fname)
        self.labels = hm_utils.load_labels_ams(self.driver)
        # Build a label dictionary
        self.label_dict = {l.identifier: l for l in self.labels}
        # Build a reverse label dictionary
        self.label_rdict = {l.name : l for l in self.labels}
        # Build a list of label names
        self.label_options = [(k, l.name) for k, l in self.label_dict.items()]

        if verbose > 0:
            print('<<< Labels loaded')


        if verbose > 0:
            print('>>> Loading the labeler')
        # Load the labeler
        self.labeler = hm_utils.build_unibo_labeler(self.labels)

        if verbose > 0:
            print('<<< Labeler loaded')

        # Load the resources
        if verbose > 0:
            print('>>> Loading resources')
        self.res_list = hm_utils.load_resources_ams(self.driver, self.labels)
        if verbose > 0:
            print('<<< Resources loaded')

        if verbose > 0:
            print('<<< HM initialized')

    # Run the matchmaking algorithm
    def run_matchmaking(self,
            query : str,
            metadata: dict = None,
            return_explanations : bool = False):
        """
        Run the matchmaking algorithm

        Run the matchmaking algorithm to obtain a ranked list of relevant
        resources.

        Parameters
        ----------
        query: str
            A textual description w.r.t. to which relevance scores will be
            computed. Ideally, this should be a few sentences long.
        metadata: list
            A list of dictionaries, containing symbolic information. Dictionary
            keys represent the field name. This information is processed ad-hoc
            depending on the specific algorithm or resource type
        return_explanations: bool
            If True, explanations are returned as a matrix of sub-scores for
            individual asset and label

        Returns
        -------
        dict:
            A dictionary containing the matchmaking results
        """

        # Prepare result data structure
        res = {'assets': [], 'warnings':[]}

        # -----------------------------------------------------------------------
        # Compute query scores
        # -----------------------------------------------------------------------

        # Retrieve raw scores
        q_scores_raw = self.labeler.score([query], return_sentences=True,
                return_amount=3)[0]
        # Take the n-th best best (to discard outliers). This approach is of
        # course less reliable with very short descriptions
        q_scores_quantile = {k: v['scores'].min()
                for k, v in q_scores_raw.items()}
        # Apply modifications if the "emph_labels" key is present in the metadata
        if metadata is not None and 'emph_labels' in metadata:
            for lname in metadata['emph_labels']:
                lidx = self.label_rdict[lname].identifier
                if lidx not in q_scores_quantile:
                    res['warnings'].append(f'label id {lidx} not found (likely due to a DB reset). Dropping to continue execution')
                else:
                    q_scores_quantile[lidx] += self.params['emph_label_increment']
        # Normalize the scores (relevance intensity is not meaningful for
        # queries; it is however meaningful for the resources)
        q_scores = {k:v / sum(q_scores_quantile.values())
                for k, v in q_scores_quantile.items()}
        # # Retrieve the only query in the list
        # q_scores = q_scores_n[0]

        # -----------------------------------------------------------------------
        # Load the resources
        # -----------------------------------------------------------------------

        #res_list = rds.get_resources()

        # -----------------------------------------------------------------------
        # Run the matchmaking algorithm
        # -----------------------------------------------------------------------

        # Build the matchmaking algorithm
        online_alg = local.build_online_matchmaking(self.res_list)
        # Run the matchmaking algorithm for the query
        mm_res = online_alg.matchOne(query_scores=q_scores,
                return_explanations=return_explanations)

        # Unpack the results
        if return_explanations:
            ranking, explanations = mm_res
        else:
            ranking = mm_res

        # -----------------------------------------------------------------------
        # Convert the results to a dictionary
        # -----------------------------------------------------------------------

        # Add the ranked assets
        for idx, score in ranking:
            rdesc = {'id': self.res_list[idx].identifier,
                    'name': self.res_list[idx].name,
                    'score': score,
                    'description': self.res_list[idx].description}
            res['assets'].append(rdesc)

        # Add the explanations, if requested
        if return_explanations:
            res['explanations'] = []
            for edict in explanations:
                edesc = [(self.label_dict[k].name, v) for k, v in edict.items()]
                edesc = sorted(edesc, key = lambda t: -t[1])
                res['explanations'].append(edesc)

        # Return results
        return res

    # Run the labeling algorithm
    def run_labeling(self, query):
        # Compute query scores
        q_scores_n = hm_utils.score_queries([query], self.labeler)
        # q_scores_raw = self.labeler.score([query], return_sentences=True, return_amount=3)
        # q_scores_quantile = [{k: v['scores'].min() for k, v in V.items()} for V in q_scores_raw]
        # q_scores_n = [{k:v / sum(d.values()) for k, v in d.items()} for d in q_scores_quantile]
        q_scores = q_scores_n[0]
        # Build a result data structure
        res = {'labels': []}
        for idx, score in sorted(q_scores.items(), key=lambda t: -t[1]):
            lbl = self.label_dict[idx]
            ldesc = {
                        'id': lbl.identifier,
                        'name': lbl.name,
                        'score': score
                    }
            res['labels'].append(ldesc)
        return res

    def get_language_options(self):
        return self.driver.get_languages(with_code=True)

    def get_country_options(self):
        return self.driver.get_countries()

    def asset_expert_access_control(self, token):
        # Configuration parameters
        ep_dir = self.params['asset_expert']
        # Perform access control
        content = ssac.check_and_read(ep_dir, token)
        return content is not None, content

    def asset_expert_get(self, expert_id, request):
        # Prepare result fields
        description = None
        country = None
        languages = None
        team_size = None
        label_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve the node and all content
        expert_data = self.driver.get_aiexperts(uuid_identifier=expert_id)

        # Populate content fields with stored values
        if expert_data is not None and 'swai__short_description' in expert_data:
            description = expert_data['swai__short_description']
        if expert_data is not None and 'swai__resides' in expert_data:
            country = expert_data['swai__resides']
        else:
            country = ''
        if expert_data is not None and 'swai__has_language' in expert_data:
            languages = expert_data['swai__has_language']
        else:
            languages = []
        if expert_data is not None and 'swai__team_size' in expert_data:
            team_size = expert_data['swai__team_size']
        else:
            team_size = ''

        # LABEL RETRIEVAL ===================================================
        # Retrieve the stored scores
        if 'swai__scored_in' in expert_data:
            scores = expert_data['swai__scored_in']
            # Adapt the format
            label_list = [(l, v) for l, v in scores.items()]
            label_list = sorted(label_list, key=lambda t: -t[1])

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
                'description': description,
                'country': country,
                'languages': languages,
                'team_size': team_size,
                'label_list': label_list
                }
        return res

    def asset_expert_post(self, expert_id, request):
        # Default values for the content fields
        description = None
        country = None
        languages = None
        team_size = None
        scores = None
        label_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve content object
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'description' in content:
            description = content['description']
        if 'country' in content:
            country = content['country'] # TODO add correctness check
        if 'languages' in content:
            if json_content:
                languages = content['languages'] # TODO add correctness check
            else:
                languages = content.getlist('languages') # TODO add correctness check
        if 'team_size' in content:
            if content['team_size'] == '':
                team_size = None
            else:
                team_size = int(content['team_size']) # TODO add correctness check

        # Recompute the scores, if enough information is provided
        if description is not None:
            # Retrieve the label list, in the internal format
            label_list = self.run_labeling(description)
            label_list = label_list['labels']
            # Convert into the AMS format
            scores = {le['name']: le['score'] for le in label_list}

        # ADJUST DATA FOR STORAGE ===========================================
        # Prepare a data structure to store in the AMS
        new_expert_data = {}
        if description is not None:
            new_expert_data['swai__short_description'] = description
        if country is not None:
            new_expert_data['swai__resides'] = country
        if languages is not None:
            if len(languages) > 0:
                new_expert_data['swai__has_language'] = languages
            else:
                new_expert_data['swai__has_language'] = languages[0]
        if team_size is not None:
            new_expert_data['swai__team_size'] = team_size
        if scores is not None:
            new_expert_data['swai__scored_in'] = scores

        # Retrive existing data for the expert
        old_expert_data = self.driver.get_aiexperts(uuid_identifier=expert_id)
        # Memorize the expert id
        expert_swai_id = old_expert_data['swai__identifier']
        # Delete the existing data
        data_to_delete = {k: v for k, v in old_expert_data.items()
                if k in new_expert_data}
        self.driver.modify_instance(expert_swai_id, delete_property=True,
                param_dict=data_to_delete)

        # Modify the resource data
        # TODO check whether the id is existent
        # expert_swai_id = self.driver.get_aiexperts(uuid_identifier=expert_id, swai_id_only=True)
        self.driver.modify_instance(expert_swai_id, delete_property=False,
                param_dict=new_expert_data)

        # ADJUST LABEL FORMAT FOR OUTPUT ===================================
        # Retrieve the stored scores
        if 'swai__scored_in' in new_expert_data:
            scores = new_expert_data['swai__scored_in']
            # Adapt the format
            label_list = [(l, v) for l, v in scores.items()]
            label_list = sorted(label_list, key=lambda t: -t[1])

        # RESULT PACKAGING ==================================================
        res = {
                'description': description,
                'country': country,
                'languages': languages,
                'team_size': team_size,
                'label_list': label_list
                }
        return res


    def query_usecase_access_control(self, token):
        # Configuration parameters
        uc_dir = self.params['matchmaking_usecase']
        # Perform access control
        content = ssac.check_and_read(uc_dir, token)
        return content is not None, content

    def query_usecase_get(self, usecase_id, request):
        # Prepare result fields
        description = None
        languages = None
        emph_labels = None
        team_size_min = None
        ai_readiness = None
        asset_list = None
        explanation_list = None

        # CONTENT RETRIEVAL =================================================
        # Storage folder
        uc_dir = self.params['matchmaking_usecase']
        # Read stored content
        content = ssac.check_and_read(uc_dir, usecase_id)

        # Populate content fields with stored values
        if content is not None and 'description' in content:
            description = content['description']
        if content is not None and 'team_size_min' in content:
            team_size_min = content['team_size_min']
        if content is not None and 'ai_readiness' in content:
            ai_readiness = content['ai_readiness']
        if content is not None and 'languages' in content:
            languages = content['languages']
        if content is not None and 'emph_labels' in content:
            emph_labels = content['emph_labels']

        # RESOURCE RETRIEVAL ================================================
        # Retrieve ranked resources
        if description is not None:
            metadata = {
                        'team_size_min' : team_size_min,
                        'ai_readiness' : ai_readiness,
                        'languages' : languages,
                        'emph_labels' : emph_labels
                    }
            mm_result = self.run_matchmaking(description, metadata=metadata,
                    return_explanations=True)
            asset_list = mm_result['assets']
            explanation_list = mm_result['explanations']

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
                'description': description,
                'languages': languages,
                'emph_labels': emph_labels,
                'team_size_min': team_size_min,
                'ai_readiness': ai_readiness,
                'asset_list': asset_list,
                'explanation_list': explanation_list
                }
        return res

    def query_usecase_post(self, usecase_id, request):
        # Prepare result fields
        description = None
        languages = None
        emph_labels = None
        team_size_min = None
        ai_readiness = None
        asset_list = None
        explanation_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve new content
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'description' in content:
            description = content['description']
        if 'team_size_min' in content:
            if content['team_size_min'] == '':
                team_size_min = None
            else:
                team_size_min = int(content['team_size_min']) # TODO add correctness check
            team_size_min = content['team_size_min']
        if 'ai_readiness' in content:
            if content['ai_readiness'] == '':
                ai_readiness = None
            else:
                ai_readiness = int(content['ai_readiness']) # TODO add correctness check
            ai_readiness = content['ai_readiness']
        if 'languages' in content:
            if json_content:
                languages = content['languages'] # TODO add correctness check
            else:
                languages = content.getlist('languages') # TODO add correctness check
        if 'emph_labels' in content:
            if json_content:
                emph_labels = emph_labels
            else:
                emph_labels = request.form.getlist('emph_labels')
        else:
            emph_labels = []

        # STORAGE ===========================================================
        # Store new information
        new_info = {}
        new_info['description'] = description
        new_info['team_size_min'] = team_size_min
        new_info['ai_readiness'] = ai_readiness
        new_info['languages'] = languages
        new_info['emph_labels'] = emph_labels
        new_info['asset_list'] = asset_list

        # Store the data
        uc_dir = self.params['matchmaking_usecase']
        ssac.check_and_store(new_info, uc_dir, usecase_id)

        # RESOURCE RETRIEVAL AND RETURN =====================================
        res = self.query_usecase_get(usecase_id, request)
        return res
