# StairwAI Asset Management System (Admin version)

This README has the requirements, steps to install and possibilities of the current StairwAI Asset Management System (AMS) API version (Admin version). Additionally, it has a bref explanation of some important concepts to understand each part of the AMS and functionalities and extra functionalities, only available for the administrator.

## Requirements

Basic system requirements:
- python>=3.6
- numpy==1.17.4
- pandas==0.25.3
- py2neo==2021.2.3

## Steps to install and run the API

1. Import the class `Driver` from `kr.StairwAIDriver.py`

2. Start to use the driver with the implemented functionalitites explained below


## General description

This documentation explains the second draft structures and capabilities, in the server AMS version exists two different APIs, User and Admin API, both of them has the same base functionalities, but admin API holds some extra capabilities. But, firstly it is important to understand some important concepts of the AMS structure, such as, the concept of class, instance, relation or attribute.

StairwAI AMS is build on the StairwAI ontology, which specify the classes available, the relations between the classes and the attributes that each of these classes can have. Additionally, StairwAI ontology also specifies the instances for some classes that non-instantiable, these non-instantiable classes can be considered as a close-set of options in which the instances of other classes can be related with.

So, mainly, in StairwAI AMS we can distinguish two type of classes: (1) instantiable (can be instantiated by the user) and (2) non-instantiable (already defined in ontology). Each of these classes have instances which can be related with, and only, instances using the Relations defined in the StairwAI ontology as Object properties. In this direction, the user can only stablish a relation between two already created instances in the AMS. Apart from Relations, the Attributes are properties of each node individually, and they are also defined in the StairwAI ontology as DataType properties, such as, description, identifier or keywords.

In the following image, you are able to observe a graphical visualization of the above explanation of the StairwAI AMS structural main concepts:
![alt text](../../stairwai_ams_schemas.png)
**Figure 1:** StairwAI AMS conceptual idea.


## Possibilities of the current AMS version
This second draft follows a similar structure as the first one, you are able to find three main capabilities, plus extra admin functionalities:
1. **Admin functionalities**: The Admin functionalities can deeply modify the AMS structure, we can find three of them:
   * Generate/modify the swai_ams.json file. This file contains the fixed structures and domain/range associated to each property of the AMS. This file can only be read by the other users, but the admin can also modify it.
   * Back-up function, this functionality stores all the added instances to a file, this file can be read after a reset to avoid the lost of data during a reset. (Under development currently)
   * Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a back-up file, restart the AMS, importing the latest StairwAI ontology available, making the changes to the *swai_ams.json* file and finally incorporating the instances available in the back-up. WARNING: currently, an AMS reset will change all the node identifiers! (Back-up part under development currently)
1. **Getters**: These functionalities allow the fast extraction of fixed data from the AMS, we could divide this data into:
   * Get non-instantiable class instances, e.g., get_countries or get_aitechniques.
   * Get general data, such as, all the classes, instantiable classes, attributes or relations of the AMS.
   * Get node information or related class information. 
2. **Insert/Delete/Modify methods**: These three methods allow the data modification in the AMS allowing inserting, deleting or modifying instances with their attributes and relationships.
3. **Predefined queries**: This set of predefined queries can handle the usual queries that during the development of the tool the developers have proposed as useful, as well as some that have been added on demand of other project partners. It is worth mentioning that the development team is still open to adding new queries on demand, so do not hesitate to contact us if that is the case.

### Admin functionalities
#### var_file_init
Property | Value
--- | ---
Description | This method create from scratch the fix data-structures of the AMS using the StairwAI Ontology structure imported. Finally, store it in a json file.
Parameter | **mod_file**: (type: boolean) boolean parameter that if it is set to False the modification in the fixed data structures will not be written in the JSON file.
Parameter | **path**: (type: string) path where the JSON file with the fixed data structures will be written.
Return | None

#### back_up (under development)
Property | Value
--- | ---
Description | this functionality stores all the added instances to a file, this file can be read after a reset to avoid the lost of data during a reset.
Parameter | **path**: (type: string) path where the BACK-UP file with the fixed data structures will be written.
Return | This method write a file with all the added instances of the AMS.

#### hard_db_reset
Property | Value
--- | ---
Description | Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a back-up file, restart the AMS, importing the latest StairwAI ontology available, making the changes to the *swai_ams.json* file and finally incorporating the instances available in the back-up. WARNING: currently, an AMS reset will change all the node identifiers!
Parameter | None
Return | None


### Getters
#### get_classes
Property | Value
--- | ---
Description | Getter of the ontology defined classes.
Parameter | **only_labels**: (type: boolean) boolean that it has to be set to True if the user only wants the instantiable class labels and set to False if the user wants the class definitions.
Return | (type: string list) A list of the available classes. 

#### get_instantiable_classes
Property | Value
--- | ---
Description | Getter of the ontology instantiable classes with its definitions.
Parameter | **only_labels**: (type: boolean) boolean that it has to be set to True if the user only wants the instantiable class labels and set to False if the user wants the class definitions.
Return | (type: string list or dictionary) A list, or dictionary, of the available instantiable classes

#### get_attributes
Property | Value
--- | ---
Description | Getter of the ontology defined attributes.
Parameter | **with_conditions**: (type: boolean) boolean that if it is active the method will return the domain of each property.
Return | (type: Pandas DT or string list) If with_conditions is true, a pandas dataframe with the properties and its domain. If with_conditions. is set to false will return the list of available attributes.

#### get_relations
Property | Value
--- | ---
Description | Getter of the ontology defined relations.
Parameter | **with_conditions**: (type: boolean) boolean that if it is active the method will return the domain/range of relation.
Return | (type: Pandas DT or string list) If with_conditions is true, a pandas dataframe with the relations and its domain/range. If with_conditions is set to false will return the list of available relations.

#### get_node_data_to_dict
Property | Value
--- | ---
Description | This query returns the node data as a dictionary.
Parameter | **node**: (type: py2neo.Node) py2neo.Node instance.
Return | (type: dictionary) Node data as a dictionary, if node = None, returns None.

#### get_class_properties
Property | Value
--- | ---
Description | This method returns a frame dataset with the assets which are identified with almost one element of the tag list passed as a parameter.
Parameter | **class_type**: (type: string) the name (label) of the class from which we want to extract the properties.
Return | type: (dictionary) A dictionary with the properties available for specific class and its requirements.

#### get_business_categories
Property | Value
--- | ---
Description | Available business categories' getter.
Parameter | None
Return | (type: string list) List of the available Business category instances.

#### get_aitechniques
Property | Value
--- | ---
Description | Available AI Techniques' getter.
Parameter | None
Return | (type: string list) List of the available AI Technique instances.

#### get_countries
Property | Value
--- | ---
Description | Available countries' getter.
Parameter | None
Return | (type: string list) List of the available Country instances.

#### get_organisation_types
Property | Value
--- | ---
Description | Available organisation types' getter.
Parameter | None
Return | (type: string list) List of the available Organisation type instances.

#### get_containers
Property | Value
--- | ---
Description | Available containers' getter.
Parameter | None
Return | (type: string list) Llist of the available Container instances.

#### get_distributions
Property | Value
--- | ---
Description | Available distributions' getter.
Parameter | None
Return | (type: string list) List of the available Distribution instances.

#### get_paces
Property | Value
--- | ---
Description | Available paces' getter.
Parameter | None
Return | (type: string list) List of the available Pace instances.

#### get_languages
Property | Value
--- | ---
Description | Available languages' getter.
Parameter | None
Return | (type: string list) List of the available Language instances.

#### get_education_levels
Property | Value
--- | ---
Description | Available education levels' getter.
Parameter | None
Return | (type: string list) List of the available Education level instances.

#### get_education_types
Property | Value
--- | ---
Description | Available education types' getter.
Parameter | None
Return | (type: string list) List of the available Education type instances.

#### get_skills
Property | Value
--- | ---
Description | Available skills' getter.
Parameter | None
Return | (type: string list) List of the available Skill instances.


### Insert/Delete/Modify methods
#### insert_instance
Property | Value
--- | ---
Description | This function allows the insertion of new instances into the database. Please check the available instantiable classes using *get_instantiable_classes()* function. Additionally, if the user wants to know the available relations and attributes for the instances of a specific class use *query_get_class_properties(class_type)* function.
Parameter | **name**: (type: string) the name of the instance that want to be inserted.
Parameter | **class_type**: (type: string) The class of the inserted instance.
Parameter | **kargs**: (type: dictionary) the instance specific parameters (relations or attributes) that want to be modified in the AMS. The name of the parameter (the key) is writen as the property name substituting the white spaces ( ) by low bars (_). The value should be the attribute content itself in the case of Attributes, and the id of the related instance in the case of the Relations.


#### delete_instance
Property | Value
--- | ---
Description | This function delete an instance, identifying it using its identifier.
Parameter | **id**: (type: integer) identifier of the organisation that have to be deleted.
Return | None


#### modify_instance
Property | Value
--- | ---
Description | This function allows the modification of any instance node in the database. Please check the available instantiable classes using *get_instantiable_classes()* function. Additionally, if the user wants to know the available relations and attributes for the instances of a specific class use *query_get_class_properties(class_type)* function.
Parameter | **id**: (type: integer) identifier of the instance which you want to modify.
Parameter | **delete_property**: (type: boolean) activate to *True* if you want to delete an attribute or a relation, set it to *False* if you want to insert new ones.
Parameter | **instr**: (do not modify) internal attribute, DO NOT MODIFY THIS PARAMETER.
Parameter | **kargs**: (type:dictionary) the instance specific parameters (relations or attributes) that want to be modified in the AMS. The name of the parameter (the key) is writen as the property name substituting the white spaces ( ) by low bars (_). The value should be the attribute content itself in the case of Attributes, and the id of the related instance in the case of the Relations.
Return | (type:dictionary) The dictionary with the data of the node modified.


#### modify_score
Property | Value
--- | ---
Description | Insert, or delete, a score in the AMS.
Parameter | **start_node_id**: (type: integer) Identifier of the instance (node) from which you want to set the new score.
Parameter | **relation**: (type: string) relation/property type. Must be *score* if you are inserting a score using as start node the AI technique, or *scored in* if you are using the AI Asset as starting node.
Parameter | **score**: (type: float) The score that you want to set (float number).
Parameter | **end_node_id**: (type: integer) identifier of the node to which you want to connect.
Parameter | **delete_property**: (type: boolean) activate to "True" if you want to delete attributes or relations, set tp "False" if you want to insert new ones.
Return | None


### Predefined queries
#### free_query
Property | Value
--- | ---
Description | This query allows a personalized query construction.
Parameter | **query**: (type: string) string with the query that is going to be used.
Return | (type: unknown) The result of the query, without post-processing.

#### query_exist_instance
Property | Value
--- | ---
Description | This query returns true if a node exists using its identifier.
Parameter | **id**: (type: integer) id of the instance searched.
Return | (type: boolean) True if it exists in the AMS or False, if it does not exist.


#### query_node_by_name_class
Property | Value
--- | ---
Description | This query returns the instance, if it exists, using the name and the class.
Parameter | **name**: (type: string) name of the instance searched.
Parameter | **ont_class**: (type: string) class of the instance searched.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_node_by_name
Property | Value
--- | ---
Description | This query returns the instance, if it exists, using the name of the instance. DO NOT USE IT IF IT IS ABSOLUTELY NECESSARY, very slow function that exponentially grow its temporal cost. Alternatively, use *query_node_by_name_class* that uses the instance class also.
Parameter | **name**: (type: string) name of the instance searched.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_node_by_id
Property | Value
--- | ---
Description | This query returns the node, if exists, using the identifier of the instance.
Parameter | **id**: (type: integer) id of the instance searched.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_instances_by_class
Property | Value
--- | ---
Description | This query returns all the instances of a specific class.
Parameter | **class_label**: (type: string) class of the instances retrieved.
Parameter | **with_inference**: (type: boolean) boolean that in the case of having a True value will return the instances that belongs to the class and its subclasses in the ontological hierarchy.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary list or py2neo.Node list) List of the instance data or instance nodes, if they exist.

#### query_class_by_id
Property | Value
--- | ---
Description | This query returns the class label of an instance, given an identifier.
Parameter | **id**: (type: integer) id of the instance searched.
Return | (type: string) The class label of the instance with the id.

#### query_related_nodes_by_relation
Property | Value
--- | ---
Description | This query returns the data of the related instances to the one with the identifier passed as a parameter, giving a specific relation.
Parameter | **id**: (type: integer) id of the instance searched.
Parameter | **relation**: (type: string) relation to be searched.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary list or py2neo.Node list) List of the instance data, or instance nodes, if they exist.

#### query_get_assets_by_tag
Property | Value
--- | ---
Description | This query returns a list of dictionaries, one dictionary for each category in categories parameter, where the associated assets are stored.
Parameter | **categories**: (type: string list) list of AI Techniques from which you want to retrieve the instances.
Parameter | **limit**: (type: integer) limit of nodes to be retrieved by the query.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Parameter | **inference_cat**: (type: boolean) boolean, set it to True if you want to infer the AI Techniques subcategories of the ones in categories parameter.
Return | (type: dictionary list) A list of dictionaries where, for each category searched, a dictionary with the category and the list of associated AI Assets are set.

#### query_get_experts_by_tag
Property | Value
--- | ---
Description | This query returns a list of dictionaries, one dictionary for each category in categories parameter, where the associated experts are stored.
Parameter | **categories**: (type: string list) list of AI Techniques from which you want to retrieve the instances.
Parameter | **limit**: (type: integer) limit of nodes to be retrieved by the query.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Parameter | **inference_cat**: (type: boolean) boolean, set it to True if you want to infer the AI Techniques subcategories of the ones in categories parameter.
Return | (type: dictionary list) A list of dictionaries where, for each category searched, a dictionary with the category and the list of associated AI Experts are set.

#### query_topk_assets
Property | Value
--- | ---
Description | This query returns an ordered assets list with, at most, K elements based on the scored assets in AI Technique.
Parameter | **category**: (type: string) AI Technique from which we want to retrieve the assets.
Parameter | **k**: (type: integer) the maximum number of elements retrieved.
Parameter | **to_dict**: (type: boolean) boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns py2neo.Node instance.
Return | (type: dictionary list or py2neo.Node list) An ordered list of assets (dictionaries{score, instance}), in the form of py2neo.Node elements or as a dictionaries.


## Contact points

- miquel.buxons@upc.edu
- javier.vazquez-salceda@upc.edu

