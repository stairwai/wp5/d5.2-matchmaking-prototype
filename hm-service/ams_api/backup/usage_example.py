from kr.StairwAIDriver import Driver

# init the driver
driver = Driver()

# examples of the getters available ######################
print(driver.get_classes())
print(driver.get_instantiable_classes())
print(driver.get_attributes(with_conditions=True))
print(driver.get_relations(with_conditions=False))
print(driver.get_node_data_to_dict(driver.query_node_by_id(driver.query_node_by_name_class('PIXANO', 'Library', to_dict=True)['swai__identifier'])))
[print(x['property']) for x in driver.get_class_properties('Course')['Relations']['properties']]
print("\n")
[print(x['property']) for x in driver.get_class_properties('Course')['Attributes']['properties']]
print(driver.get_business_categories())
print(driver.get_aitechniques())
print(driver.get_countries())
print(driver.get_containers())
print(driver.get_education_types())
print(driver.get_organisation_types())
print(driver.get_education_levels())
print(driver.get_distributions())
print(driver.get_paces())
print(driver.get_languages())
print(driver.get_skills())

# examples of insertions/modifications/deletions of AMS instances ######################
# AI Artifact -> Algorithm
node = driver.insert_instance("Example", class_type='Algorithm', short_description="Example of description",
                              keyword="Example",
                              license_of_distribution="licence of the example", gdpr_requirements='GDPR requirements',
                              has_language=driver.query_node_by_name_class('Greek', 'Language', to_dict=True)[
                                  'swai__identifier'],
                              wrapped_in=driver.query_node_by_name_class('jupyter notebook', 'Container', to_dict=True)[
                                  'swai__identifier'],
                              distributed_as=driver.query_node_by_name_class('SAAS', 'Distribution', to_dict=True)[
                                  'swai__identifier'],
                              applicable_to=
                              driver.query_node_by_name_class('Agriculture', 'Business categories', to_dict=True)[
                                  'swai__identifier'],
                              apply=driver.query_node_by_name_class('Machine learning', 'AI Technique', to_dict=True)[
                                  'swai__identifier'],
                              trustworthy_AI='Trustworthy AI reqs')
driver.modify_instance(node['swai__identifier'], delete_property=False,
                       detailed_description="insert a detailed description")
print(driver.query_node_by_name_class(name="Example", ont_class="Algorithm"))
driver.delete_instance(node['swai__identifier'])

# AI expert
node = driver.insert_instance("Example", class_type='AI expert', short_description="Example of description",
                              keyword="Example",
                              has_language=driver.query_node_by_name_class('Greek', 'Language', to_dict=True)[
                                  'swai__identifier'],
                              has_organisation_type=
                              driver.query_node_by_name_class('Startup company', 'Organisation Type', to_dict=True)[
                                  'swai__identifier'],
                              has_educational_level=
                              driver.query_node_by_name_class('Master', 'Education level', to_dict=True)[
                                  'swai__identifier'],
                              resides=driver.query_node_by_name_class('Kazakhstan', 'Country', to_dict=True)[
                                  'swai__identifier'],
                              experience_in=
                              driver.query_node_by_name_class('Agriculture', 'Business categories', to_dict=True)[
                                  'swai__identifier'],
                              expertise_in=
                              driver.query_node_by_name_class('Machine learning', 'AI Technique', to_dict=True)[
                                  'swai__identifier'])
driver.modify_instance(node['swai__identifier'], delete_property=False, available_expert=True)
print(driver.query_node_by_name_class(name="Example", ont_class="AI expert"))
driver.delete_instance(node['swai__identifier'])

# Course
node = driver.insert_instance("Example", class_type='Course', short_description="Example of description",
                              keyword="Example",
                              has_language=driver.query_node_by_name_class('Greek', 'Language', to_dict=True)[
                                  'swai__identifier'],
                              has_educational_level=
                              driver.query_node_by_name_class('Master', 'Education level', to_dict=True)[
                                  'swai__identifier'],
                              imparted_in=driver.query_node_by_name_class('Kazakhstan', 'Country', to_dict=True)[
                                  'swai__identifier'],
                              is_paced_as=driver.query_node_by_name_class('Full-time', 'Pace', to_dict=True)[
                                  'swai__identifier'],
                              imparted_as=driver.query_node_by_name_class('On-site', 'Education type', to_dict=True)[
                                  'swai__identifier'])
driver.modify_instance(node['swai__identifier'], delete_property=False, url='https://example.exp')
print(driver.query_node_by_name_class(name="Example", ont_class="Course"))
driver.delete_instance(node['swai__identifier'])

# Node id specification
nlp_id = driver.query_node_by_name_class('Natural language processing', 'AI Technique', to_dict=True)['swai__identifier']
ex1_id = driver.query_node_by_name_class('PIXANO', 'Library', to_dict=True)['swai__identifier']
ex2_id = driver.query_node_by_name_class('PREMIN', 'Library', to_dict=True)['swai__identifier']
ex3_id = driver.query_node_by_name_class('ABELE', 'Library', to_dict=True)['swai__identifier']

# Insert/delete a score examples ######################
driver.modify_score(nlp_id, relation='score', end_node_id=ex1_id, score=0.5)  # insert
driver.modify_score(nlp_id, relation='score', end_node_id=ex2_id, score=0.7)  # insert
driver.modify_score(nlp_id, relation='score', end_node_id=ex3_id, score=0.2)  # insert
print(driver.query_topk_assets('Natural language processing', 5, to_dict=True))  # TOPk node scores connected to NLP
driver.modify_score(nlp_id, relation='score', end_node_id=ex1_id, delete_property=True)  # delete
driver.modify_score(nlp_id, relation='score', end_node_id=ex2_id, delete_property=True)  # delete
driver.modify_score(nlp_id, relation='score', end_node_id=ex3_id, delete_property=True)  # delete

# examples of calls to the queries available ######################
print(driver.query_class_by_id(id=nlp_id))
print(driver.query_node_by_id(id=ex2_id, to_dict=False))
print(driver.query_node_by_name_class(name='KENN', ont_class='Library', to_dict=True))
print(driver.free_query('MATCH (n:owl__NamedIndividual) WHERE n.swai__identifier = {0} RETURN n'.format(ex1_id)))
print(driver.query_exist_instance(id=ex1_id))
print(driver.query_instances_by_class('Tool', with_inference=True))
print(driver.query_related_nodes_by_relation(id=ex3_id, relation='owner'))
print(driver.query_get_assets_by_tag(['Machine learning'], 5, inference_cat=False)[0]['nodes'])
print(driver.query_get_experts_by_tag(['Machine learning'], 4, inference_cat=True))
