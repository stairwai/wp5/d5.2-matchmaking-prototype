# TODO: solve this import
# SET THE PATH-TO stairwai-ams-admin/kr/StairwAINeo4jDriver.py
MODULE_PATH = "/home/emisino/GitLab/internal-prototype/web-service/stairwai-ams-admin/kr/StairwAINeo4jDriver.py"
MODULE_NAME = "StairwAINeo4jDriver"
import importlib.util
import sys
import os

spec = importlib.util.spec_from_file_location(MODULE_NAME, MODULE_PATH)
module = importlib.util.module_from_spec(spec)
sys.modules[spec.name] = module
spec.loader.exec_module(module)


#from kr.StairwAINeo4jDriver import NeoDriver
from StairwAINeo4jDriver import NeoDriver


def get_env_var(name, default=None):
    if name in os.environ:
        return os.environ[name]
    else:
        return default


class Driver:
    def __init__(self):
        # load the credentials of the database (normal port 7687)
        bolt_url = get_env_var('NEO4J_URL', 'bolt://localhost:7687')
        user = get_env_var('NEO4J_USER', 'neo4j')
        password = get_env_var('NEO4J_PASSWORD', 's3cr3t')

        # bolt_url = 'bolt://localhost:7687'
        # user = 'neo4j'
        # password = 's3cr3t'
        # Parameters required for the connection between neo4j server
        self.driver = NeoDriver(bolt_url, user, password)

    def hard_db_reset(self):
        """
        Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI
        ontology update. This function automatically stores all the inserted instances in a back-up file, restart the
        AMS, importing the latest StairwAI ontology available, making the changes to the *swai_ams.json* file and
        finally incorporating the instances available in the back-up. WARNING: currently, an AMS reset will change all
        the node identifiers!
        """
        self.driver.hard_db_reset()

    def var_file_init(self, mod_file=True, path=None):
        """
        This method create from scratch the fix data-structures of the AMS using the StairwAI Ontology structure
        imported. Finally, store it in a json file.
        :param mod_file: boolean parameter that if it is set to False the modification in the fixed data structures will
        not be written in the JSON file.
        :param path: path where the JSON file with the fixed data structures will be written.
        """
        self.driver.var_file_init(mod_file=mod_file, path=path)

    # GETTERS #
    def get_classes(self, only_labels=False):
        """
        Getter of the ontology defined classes.
        :param only_labels: boolean that it has to be set to True if the user only wants the instantiable class labels
        and set to False if the user wants the class definitions.
        :return: a list of the available classes.
        """
        return self.driver.get_instantiable_classes(only_instantiable=False, only_labels=only_labels)

    def get_instantiable_classes(self, only_labels=False):
        """
        Getter of the ontology instantiable classes with its definitions.
        :param only_labels: boolean that it has to be set to True if the user only wants the instantiable class labels
        and set to False if the user wants the class definitions.
        :return: a list, or dictionary, of the available instantiable classes
        """
        return self.driver.get_instantiable_classes(only_instantiable=True, only_labels=only_labels)

    def get_attributes(self, with_conditions: bool):
        """
        Getter of the ontology defined attributes.
        :param with_conditions: boolean that if it is active the method will return the domain of each property.
        :return: if with_conditions is true, a pandas dataframe with the properties and its domain. If with_conditions.
        is set to false will return the list of available attributes.
        """
        return self.driver.get_attributes(with_conditions)

    def get_relations(self, with_conditions: bool):
        """
        Getter of the ontology defined relations.
        :param with_conditions: boolean that if it is active the method will return the domain/range of relation.
        :return: if with_conditions is true, a pandas dataframe with the relations and its domain/range.
        If with_conditions is set to false will return the list of available relations.
        """
        return self.driver.get_relations(with_conditions)

    def get_node_data_to_dict(self, node):
        """
        This query returns the node data as a dictionary.
        :param node: py2neo.Node instance.
        :return: Node data as a dictionary, if node = None, returns None.
        """
        return self.driver.get_node_data_to_dict(node)

    def get_class_properties(self, class_type: str):
        """
        This method returns a frame dataset with the assets which are identified with almost one element of the tag list
        passed as a parameter.
        :param class_type: the name (label) of the class from which we want to extract the properties.
        :return: a dictionary with the properties available for specific class and its requirements.
        """
        return self.driver.get_class_properties(class_type)

    def get_business_categories(self):
        """
        Available business categories' getter.
        :return: list of the available Business category instances.
        """
        return self.driver.get_instances_by_class('BusinessCategories')

    def get_aitechniques(self):
        """
        Available AI Techniques' getter.
        :return: list of the available AI Technique instances.
        """
        return self.driver.get_instances_by_class('AITechnique')

    def get_countries(self):
        """
        Available countries' getter.
        :return: list of the available Country instances.
        """
        return self.driver.get_instances_by_class('Country')

    def get_organisation_types(self):
        """
        Available organisation types' getter.
        :return: list of the available Organisation type instances.
        """
        return self.driver.get_instances_by_class('OrganisationType')

    def get_containers(self):
        """
        Available containers' getter.
        :return: list of the available Container instances.
        """
        return self.driver.get_instances_by_class('Container')

    def get_distributions(self):
        """
        Available distributions' getter.
        :return: list of the available Distribution instances.
        """
        return self.driver.get_instances_by_class('Distribution')

    def get_paces(self):
        """
        Available paces' getter.
        :return: list of the available Pace instances.
        """
        return self.driver.get_instances_by_class('Pace')

    def get_languages(self):
        """
        Available languages' getter.
        :return: list of the available Language instances.
        """
        return self.driver.get_instances_by_class('Language')

    def get_education_levels(self):
        """
        Available education levels' getter.
        :return: list of the available Education level instances.
        """
        return self.driver.get_instances_by_class('EducationLevel')

    def get_education_types(self):
        """
        Available education types' getter.
        :return: list of the available Education type instances.
        """
        return self.driver.get_instances_by_class('EducationType')

    def get_skills(self):
        """
        Available skills' getter.
        :return: list of the available Skill instances.
        """
        return self.driver.get_instances_by_class('Skill')

    # INSTANCE MODIFICATORS #
    def insert_instance(self, name: str, class_type, **kwargs):
        """
        This function allows the insertion of new instances into the database. Please check the available instantiable
        classes using 'get_instantiable_classes()' function. Additionally, if the user wants to know the available
        relations and attributes for the instances of a specific class use 'query_get_class_properties(class_type)'
        function.
        :param name: the name of the instance that want to be inserted.
        :param class_type: The class of the inserted instance.
        :param kwargs: the instance specific parameters (relations or attributes) that want to be modified in the AMS.
        The name of the parameter (the key) is writen as the property name substituting the white spaces ( ) by low-bars
        (_). The value should be the attribute content itself in the case of Attributes, and the id of the related
        instance in the case of the Relations.
        :return: the dictionary with the data of the node inserted.
        """
        # insert the new instance into the database
        node = self.driver.insert_node(name, class_type)
        return self.modify_instance(node.identity, instr=True, identifier=node.identity, **kwargs)

    def delete_instance(self, id: int):
        """
        This function delete an instance, identifying it using its identifier.
        :param id: identifier of the organisation that have to be deleted.
        """
        self.driver.delete_node(id)

    def modify_instance(self, id: int, delete_property: bool = False, instr=False, **kwargs):
        """
        This function allows the modification of any instance node in the database. Please check the available
        instantiable classes using 'get_instantiable_classes()' function. Additionally, if the user wants to know the
        available relations and attributes for the instances of a specific class use
        'query_get_class_properties(class_type)' function.
        :param id: identifier of the instance which you want to modify.
        :param delete_property: activate to "True" if you want to delete an attribute or a relation, set it to "False"
        if you want to insert new ones.
        :param instr: internal attribute, DO NOT MODIFY THIS PARAMETER.
        :param kwargs: the instance specific parameters (relations or attributes) that want to be modified in the AMS.
        The name of the parameter (the key) is writen as the property name substituting the white spaces ( ) by low-bars
        (_). The value should be the attribute content itself in the case of Attributes, and the id of the related
        instance in the case of the Relations.
        :return: the dictionary with the data of the node modified.
        """
        for key, value in kwargs.items():
            if value is None:
                continue
            prop = key.replace("_", " ")
            df_row = self.driver.properties.loc[self.driver.properties["label"] == prop]
            # if property specified by the user not found
            if df_row.empty:
                if instr and not delete_property:
                    self.delete_instance(id=id)
                raise Exception(
                    "Property {0} is not a property of the database, referred to instance with the ID: {1}".format(prop, id))

            # if it is an ObjectProperty (Relations)
            if df_row["type"].values[0] == "ObjectProperty":
                # if the class of the node to be related with, is not found
                try:
                    # if the modification is a deletion
                    if delete_property:
                        self.driver.delete_property(id, prop, end_node_id=value)
                    # if the modification is an insertion
                    else:
                        self.driver.insert_property(id, prop, end_node_id=value)
                except Exception:
                    # if bad insertion parameters, insertion cancelled and delete trash node
                    if instr and not delete_property:
                        self.delete_instance(id=id)
                    raise sys.exc_info()[1]

            # if it is a DataTypeProperty (Attributes)
            else:
                try:
                    # if the modification is a deletion
                    if delete_property:
                        self.driver.delete_property(id, prop)
                    # if the modification is an insertion
                    else:
                        self.driver.insert_property(id, prop, value=value)
                except Exception:
                    # if bad insertion parameters, insertion cancelled and delete trash node
                    if instr and not delete_property:
                        self.delete_instance(id=id)
                    raise sys.exc_info()[1]
        return self.driver.query_node_by_id(id, to_dict=True)

        # Mandatory condition example (it will be necessary in the future maybe)
        # if business_cat is not None and len( self.driver.query_node_giving_node_relation(name, class_node, "operates in")) == 0:

    def modify_score(self, start_node_id: int, relation: str, end_node_id: int, score: float = None,  delete_property: bool = False):
        """
        Insert, or delete, a score in the AMS.
        :param start_node_id: Identifier of the instance (node) from which you want to set the new score.
        :param relation: relation/property type. Must be 'score' if you are inserting a score using as start node the
        AI technique, or 'scored in' if you are using the AI Asset as starting node.
        :param score: The score that you want to set (float number).
        :param end_node_id: identifier of the node to which you want to connect.
        :param delete_property: activate to "True" if you want to delete attributes or relations, set tp "False" if you
        want to insert new ones.
        """
        relation = relation.replace("_", " ")
        if relation not in ['score', 'scored in']:
            raise Exception(
                "The relation should be or 'score' (AI Technique -> AI Asset) or 'scored_in' (AI Asset -> AI Technique)")

        if score is None and not delete_property:
            raise Exception("You should specify the score to insert.")

        try:
            # if the modification is a deletion
            if delete_property:
                self.driver.delete_property(id=start_node_id, relation=relation, end_node_id=end_node_id)
            # if the modification is an insertion
            else:
                self.driver.insert_property(id=start_node_id, relation=relation, end_node_id=end_node_id, value=score)
        except Exception:
            raise sys.exc_info()[1]

    # QUERIES #
    def free_query(self, query: str):
        """
        This query allows a personalized query construction.
        :param query: string with the query that is going to be used
        :return: the result of the query, without post-processing.
        """
        return self.driver.free_query(query)

    def query_exist_instance(self, id: int):
        """
        This query returns true if a node exists using its identifier.
        :param id: id of the instance searched.
        :return: True if it exists in the AMS or False, if it does not exist.
        """
        return self.driver.query_exist_instance(id)

    def query_node_by_name_class(self, name: str, ont_class: str, to_dict=False):
        """
        This query returns the instance, if it exists, using the name and the class.
        :param name: name of the instance searched.
        :param ont_class: class of the instance searched.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: instance data or instance node, if it exists.
        """
        return self.driver.query_node_by_name_class(db_name=name, db_class=ont_class, to_dict=to_dict)

    def query_node_by_name(self, name: str, to_dict=False):
        """
        This query returns the instance, if it exists, using the name of the instance. DO NOT USE IT IF IT IS ABSOLUTELY
        NECESSARY, very slow function that exponentially grow its temporal cost. Alternatively, use
        'query_node_by_name_class' that uses the instance class also.
        :param name: name of the instance searched.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: instance data or instance node, if it exists.
        """
        return self.driver.query_node_by_name(name=name, to_dict=to_dict)

    def query_node_by_id(self, id: int, to_dict=False):
        """
        This query returns the node, if exists, using the identifier of the instance.
        :param id: id of the instance searched.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: instance data or instance node, if it exists.
        """
        return self.driver.query_node_by_id(id, to_dict)

    def query_instances_by_class(self, class_label, with_inference:bool, to_dict=False):
        """
        This query returns the instances of a specific class
        :param class_label: class of the instances retrieved
        :param with_inference: boolean that in the case of having a True value will return the instances that belongs
        to the class and its subclasses in the ontological hierarchy.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: list of the instance data or instance nodes, if they exist.
        """
        return self.driver.query_instances_by_class(class_label, with_inference=with_inference, to_dict=to_dict)

    def query_class_by_id(self, id: int):
        """
        This query returns the class label of an instance, given an identifier.
        :param id: identifier of the instance.
        :return: The class label of the instance with the id.
        """
        return self.driver.class_4_instance(id=id)

    def query_related_nodes_by_relation(self, id: int, relation: str, to_dict=False):
        """
        This query returns the data of the related instances to the one with the identifier passed as a parameter,
        giving a specific relation.
        :param id: identifier of the instance.
        :param relation: relation to be searched.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: list of the instance data, or instance nodes, if they exist.
        """
        return self.driver.query_related_nodes_by_relation(id, relation, to_dict)

    def query_get_assets_by_tag(self, categories: list, limit: int, inference_cat: bool = False, to_dict: bool = False):
        """
        This query returns a list of dictionaries, one dictionary for each category in categories parameter, where the
        associated assets are stored.
        :param categories: list of AI Techniques from which you want to retrieve the instances.
        :param limit: limit of nodes to be retrieved by the query.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :param inference_cat: boolean, set it to True if you want to infer the AI Techniques subcategories of the ones
        in categories parameter.
        :return: a list of dictionaries where, for each category searched, a dictionary with the category and the list
        of associated AI Assets are set.
        """
        return self.driver.query_get_assets_by_tag(categories, limit, inference_cat, to_dict)

    def query_get_experts_by_tag(self, categories: list, limit: int, inference_cat: bool = False, to_dict: bool = False):
        """
        This query returns a list of dictionaries, one dictionary for each category in categories parameter, where the
        associated experts are stored.
        :param categories: list of AI Techniques from which you want to retrieve the instances.
        :param limit: limit of nodes to be retrieved by the query.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :param inference_cat: boolean, set it to True if you want to infer the AI Techniques subcategories of the ones
        in categories parameter.
        :return: a list of dictionaries where, for each category searched, a dictionary with the category and the list
        of associated AI Experts are set.
        """
        return self.driver.query_get_experts_by_tag(categories, limit, inference_cat, to_dict)

    def query_topk_assets(self, category: str, k: int, to_dict: bool = False):
        """
        This query returns an ordered assets list with, at most, K elements based on the scored assets in AI Technique.
        :param category: AI Technique from which we want to retrieve the assets.
        :param k: the maximum number of elements retrieved.
        :param to_dict: boolean parameter that, if it is True, returns the node data as a dictionary. Otherwise, returns
        py2neo.Node instance.
        :return: An ordered list of assets (dictionaries{score, instance}), in the form of py2neo.Node elements or
        as a dictionaries.
        """
        return self.driver.query_topk_assets(category, k, to_dict)
