# StairwAI Asset Management System (Admin version)

This README has the requirements, steps to install and possibilities of the current StairwAI Asset Management System (AMS) API version (Admin version). Additionally, it has a bref explanation of some important concepts to understand each part of the AMS and functionalities and extra functionalities, only available for the administrator.

## Requirements

Basic system requirements:
- python>=3.6
- numpy==1.17.4
- pandas==0.25.3
- py2neo==2021.2.3

## Steps to install and run the API

1. Import the class `Driver` from `kr.StairwAIDriver.py`

2. Start to use the driver with the implemented functionalitites explained below


## General description

This documentation explains the second draft structures and capabilities, in the server AMS version exists two different APIs, User and Admin API, both of them has the same base functionalities, but admin API holds some extra capabilities. But, firstly it is important to understand some important concepts of the AMS structure, such as, the concept of class, instance, relation or attribute.

StairwAI AMS is build on the StairwAI ontology, which specify the classes available, the relations between the classes and the attributes that each of these classes can have. Additionally, StairwAI ontology also specifies the instances for some classes that non-instantiable, these non-instantiable classes can be considered as a close-set of options in which the instances of other classes can be related with.

So, mainly, in StairwAI AMS we can distinguish two type of classes: (1) instantiable (can be instantiated by the user) and (2) non-instantiable (already defined in ontology). Each of these classes have instances which can be related with, and only, instances using the Relations defined in the StairwAI ontology as Object properties. In this direction, the user can only stablish a relation between two already created instances in the AMS. Apart from Relations, the Attributes are properties of each node individually, and they are also defined in the StairwAI ontology as DataType properties, such as, description, identifier or keywords.

In the following image, you are able to observe a graphical visualization of the above explanation of the StairwAI AMS structural main concepts:
![alt text](../../stairwai_ams_schemas.png)
**Figure 1:** StairwAI AMS conceptual idea.


## Possibilities of the current AMS version
This second draft follows a similar structure as the first one, you are able to find three main capabilities, plus extra admin functionalities:
1. **Admin functionalities**: The Admin functionalities can deeply modify the AMS structure, we can find three of them:
   * Generate/modify the swai_ams.json file. This file contains the fixed structures and domain/range associated to each property of the AMS. This file can only be read by the other users, but the admin can also modify it.
   * Back-up function, this functionality stores all the added instances to a file, this file can be read after a reset to avoid the lost of data during a reset. (Under development currently)
   * Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a back-up file, restart the AMS, importing the latest StairwAI ontology available, making the changes to the *swai_ams.json* file and finally incorporating the instances available in the back-up. WARNING: currently, an AMS reset will change all the node identifiers! (Back-up part under development currently)
1. **Getters**: These functionalities allow the fast extraction of fixed data from the AMS, we could divide this data into:
   * Get non-instantiable class instances, e.g., get_countries or get_aitechniques.
   * Get general data, such as, all the classes, instantiable classes, attributes or relations of the AMS.
   * Get node information or related class information. 
2. **Insert/Delete/Modify methods**: These three methods allow the data modification in the AMS allowing inserting, deleting or modifying instances with their attributes and relationships.
3. **Predefined queries**: This set of predefined queries can handle the usual queries that during the development of the tool the developers have proposed as useful, as well as some that have been added on demand of other project partners. It is worth mentioning that the development team is still open to adding new queries on demand, so do not hesitate to contact us if that is the case.

### Admin functionalities
#### var_file_init
Property | Value
--- | ---
Description | This method creates from scratch the model data-structures of the AMS using the StairwAI Ontology structure imported. Finally, store it in a JSON file.
Parameter | **mod_file**: (type: boolean) whether the modification in the fixed data structures will be written in the JSON file.
Return | None

#### create_backup
Property | Value
--- | ---
Description | This method creates a StairwAI AMS backup, with the information extracted from the nodes and relations it creates two pickle files where these data are stored. Use 'read_backup()' function in order to store again the files' information to the database.
Parameter | None
Return | None

#### read_backup
Property | Value
--- | ---
Description | This method read the pickle files with the AMS instances backup generated by the method create_backup(), and load the instances in the AMS. Additionally, in deletes the pickle files.
Parameter | None
Return | None

#### hard_db_reset
Property | Value
--- | ---
Description | Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a backup file, restarts the AMS, imports the latest StairwAI ontology available, makes the changes to the swai_ams.json file and finally incorporates the instances available in the backup. WARNING: currently, an AMS reset will change all the node identifiers!
Parameter | **with_backup**: (type: boolean) Boolean parameter, if it is set to true a backup of the AMS instances is done before the reset, after it the instances are loaded to the AMS again. If it is set to False, the AMS INSTANCES WILL BE COMPLETELY AND PERMANENTLY DELETED.
Return | None


### Getters
#### get_classes
Property | Value
--- | ---
Description | Getter of the ontology defined classes.
Parameter | **only_labels**: (type: boolean) specifies whether the user only wants the instantiable class labels (True) or if the user wants the class definitions (False).
Return | (type: string list) A list of the available classes.

#### get_instantiable_classes
Property | Value
--- | ---
Description | Getter of the ontology instantiable classes with their definitions.
Parameter | **only_labels**: (type: boolean) specifies whether the user only wants the instantiable class labels (True) or if the user wants the class definitions (False).
Return | (type: string list or dictionary) A list, or dictionary, of the available instantiable classes

#### get_attributes
Property | Value
--- | ---
Description | Getter of the ontology defined attributes.
Parameter | **with_conditions**: (type: boolean) whether the method must return the domain of each property.
Return | (type: Pandas DT or string list) If with_conditions is True, a pandas data frame with the properties and its domain. Otherwise, it will return the list of available attributes.

#### get_relations
Property | Value
--- | ---
Description | Getter of the ontology defined relations.
Parameter | **with_conditions**: (type: boolean) whether the method must return the domain of each property.
Return | (type: Pandas DT or string list) If with_conditions is True, a pandas' data frame with the relations and its domain/range. Otherwise, it will return the list of available relations.

#### get_node_data_to_dict
Property | Value
--- | ---
Description | This query returns the node data as a dictionary.
Parameter | **node**: (type: py2neo.Node) py2neo.Node instance.
Return | (type: dictionary) Node data as a dictionary, if node = None, returns None.

#### get_class_properties
Property | Value
--- | ---
Description | This method returns a dictionary with the properties, and its definitions, of a specific type of class.
Parameter | **class_type**: (type: string) the name (label) of the class from which to extract the properties.
Return | type: (dictionary) A dictionary with the properties available for a specific class and its requirements.

#### get_business_categories
Property | Value
--- | ---
Description | Available business categories' getter.
Parameter | **with_details**: if True it returns the descriptions, additionally to the labels of the business categories as a dictionary.
Return | (type: string list) List of the available Business category instances.

#### get_aitechniques
Property | Value
--- | ---
Description | Available AI Techniques' getter.
Parameter | **with_details**: if True it returns the descriptions, additionally to the labels of the business categories as a dictionary.
Return | (type: string list) List of the available AI Technique instances.

#### get_countries
Property | Value
--- | ---
Description | Available countries' getter.
Parameter | None
Return | (type: string list) List of the available Country instances.

#### get_organisation_types
Property | Value
--- | ---
Description | Available organisation types' getter.
Parameter | None
Return | (type: string list) List of the available Organisation type instances.

#### get_containers
Property | Value
--- | ---
Description | Available containers' getter.
Parameter | None
Return | (type: string list) Llist of the available Container instances.

#### get_distributions
Property | Value
--- | ---
Description | Available distributions' getter.
Parameter | None
Return | (type: string list) List of the available Distribution instances.

#### get_paces
Property | Value
--- | ---
Description | Available paces' getter.
Parameter | None
Return | (type: string list) List of the available Pace instances.

#### get_languages
Property | Value
--- | ---
Description | Available languages' getter.
Parameter | None
Return | (type: string list) List of the available Language instances.

#### get_education_levels
Property | Value
--- | ---
Description | Available education levels' getter.
Parameter | None
Return | (type: string list) List of the available Education level instances.

#### get_education_types
Property | Value
--- | ---
Description | Available education types' getter.
Parameter | None
Return | (type: string list) List of the available Education type instances.

#### get_skills
Property | Value
--- | ---
Description | Available skills' getter.
Parameter | None
Return | (type: string list) List of the available Skill instances.


### Insert/Delete/Modify methods
#### insert_instance
Property | Value
--- | ---
Description | This function allows the insertion of new instances into the database. To check the set of available instantiable classes, please call *get_instantiable_classes()* function. Additionally, to know the available relations and attributes for the instances of a specific class, call *query_get_class_properties(class_type)* function.
Parameter | **name**: (type: string) the name of the instance that is going to be inserted.
Parameter | **class_type**: (type: string) The class of the inserted instance.
Parameter | **param_dict**: (type: dictionary) the instance specific dictionary (relations or attributes) that are going to be modified in the AMS. This dictionary would only accept the specific relations/attributes to the class of the node, to check it, call *query_get_class_properties(class_type)* where you could find the relations/attributes for the class, a description of them and their range.
Return | (type:dictionary) The dictionary with the data of the inserted.

#### delete_instance
Property | Value
--- | ---
Description | This function deletes the instance specified by its identifier.
Parameter | **neo_id**: (type: integer) identifier of the instance that has to be deleted.
Return | None


#### modify_instance
Property | Value
--- | ---
Description | This function allows the modification of any instance node in the database. To check the available instantiable classes, call *get_instantiable_classes()* function. Additionally, to check the available relations and attributes for the instances of a specific class, call *query_get_class_properties(class_type)* function.
Parameter | **neo_id**: (type: integer) identifier of the instance which is to be modified.
Parameter | **delete_property**: (type: boolean) whether the attribute or a relation is to be deleted (True), or inserted (False).
Parameter | **param_dict**: (type:dictionary) the instance specific dictionary (relations or attributes) that are going to be modified in the AMS. This dictionary would only accept the specific relations/attributes to the class of the node, to check it, call *query_get_class_properties(class_type)* where you could find the relations/attributes for the class, a description of them and their range.
Return | (type:dictionary) The dictionary with the data of the node modified.


#### score_list_modifier
Property | Value
--- | ---
Description | Insert a list of scores in the AMS using a list of dictionaries.
Parameter | **score_list**: (type: dictionary list) list of dictionaries, where each dictionary is a score insertion, from each dictionary, it is required the fields of 'start_node_id' (with the ID of the relation starting instance), 'end_node_id' (with the ID of the relation ending instance) and 'score' (field that specify the score value).
Return | None

#### modify_score
Property | Value
--- | ---
Description | Insert, or delete, a score in the AMS.
Parameter | **start_node_id**: (type: integer) Identifier of the instance (node) from which the new score is to be set.
Parameter | **score**: (type: float) The score value.
Parameter | **end_node_id**: (type: integer) identifier of the node to connect.
Parameter | **delete_property**: (type: boolean) whether to delete attributes or relations (True), or to insert new ones (False).
Return | None


### Predefined queries
#### free_query
Property | Value
--- | ---
Description | This query allows a personalized query construction.
Parameter | **query**: (type: string) query: string with the Cypher code command (query) that is going to be used.
Return | (type: unknown) The result of the query, without post-processing.

#### query_exist_instance
Property | Value
--- | ---
Description | This query returns true if a specific instance exists in the database.
Parameter | **neo_id**: (type: integer) ID of the instance searched.
Return | (type: boolean) True if the instance identified by id exists in the AMS, False otherwise.

#### query_if_relation_exists
Property | Value
--- | ---
Description | This query returns if a specific relation exists in the AMS.
Parameter | **start_node_id**: (type: integer) identifier of the starting instance in the relation (all the relations in the AMS are directed).
Parameter | **relation**: (type: string) relation to be searched.
Parameter | **end_node_id**: (type: integer) identifier of the ending instance in the relation (all the relations in the AMS are directed).
Return | (type: boolean) A boolean with the result of the query.


#### query_node_by_name_class
Property | Value
--- | ---
Description | This query returns an instance, if it exists, based on its name and the class.
Parameter | **name**: (type: string) name of the instance searched.
Parameter | **ont_class**: (type: string) class of the instance searched.
Parameter | **to_dict**: (type: boolean) whether the result must be returned as a dictionary (True) or apy2neo.Node instance (False).
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_node_by_name
Property | Value
--- | ---
Description | This query returns an instance, if it exists, given its name. WARNING: DO NOT USE UNLESS IT IS ABSOLUTELY NECESSARY, very slow function with exponential time cost. Alternatively, use query_node_by_name_class that uses the instance class also.
Parameter | **name**: (type: string) name of the instance searched.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_node_by_uuid
Property | Value
--- | ---
Description | This query returns an instance, if it exists, based on its uuid and the class. Only AI Asset subclasses have uuid.
Parameter | **uuid_db**: (type: string) UUID identifier of the instance searched.
Parameter | **ont_class**: (type: string) class of the instance searched.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_node_by_id
Property | Value
--- | ---
Description | This query returns the node, if it exists, given its identifier.
Parameter | **neo_id**: (type: integer) id of the instance searched.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary or py2neo.Node) Instance data or instance node, if it exists.

#### query_instances_by_class
Property | Value
--- | ---
Description | This query returns all the instances of a specific class.
Parameter | **class_label**: (type: string) class name.
Parameter | **with_inference**: (type: boolean) in the case of True value in this parameter, the query will retrieve the instances of the class specified as a parameter and all the instances of its subclasses also.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary list or py2neo.Node list) List of the instance data or instance nodes, if they exist.

#### query_class_by_id
Property | Value
--- | ---
Description | This query returns the class label of an instance, given an identifier.
Parameter | **neo_id**: (type: integer) id of the instance searched.
Return | (type: string) The class label of the instance with the id.

#### query_related_nodes_by_relation
Property | Value
--- | ---
Description | This query returns the data of the instances related to the given identifier and relation.
Parameter | **neo_id**: (type: integer) id of the instance searched.
Parameter | **relation**: (type: string) relation to be searched.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary list or py2neo.Node list) List of the instance data, or instance nodes, if they exist.

#### query_get_assets_by_tag
Property | Value
--- | ---
Description | This query returns a list of AI Artifacts that belongs to a specific AI Technique.
Parameter | **categories**: (type: string list) list of AI Techniques from which to retrieve the instances.
Parameter | **limit**: (type: integer) limit of nodes to be retrieved by the query.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Parameter | **inference_cat**: (type: boolean) whether to infer the AI Techniques subcategories of the ones in categories parameter.
Return | (type: dictionary list) A list of dictionaries where, for each category searched, a dictionary is retrieved with the artifact and the category that belongs to.

#### query_get_experts_by_tag
Property | Value
--- | ---
Description | This query returns a list of AI Experts that belongs to a specific AI Technique.
Parameter | **categories**: (type: string list) list of AI Techniques from which to retrieve the instances.
Parameter | **limit**: (type: integer) limit of nodes to be retrieved by the query.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Parameter | **inference_cat**: (type: boolean) whether to infer the AI Techniques subcategories of the ones in categories parameter.
Return | (type: dictionary list) A list of dictionaries where, for each category searched, a dictionary is retrieved with the expert and the category that belongs to.

#### query_topk_assets
Property | Value
--- | ---
Description | This query returns an ordered asset list with, at most, K elements based on the scored assets in AI Technique.
Parameter | **category**: (type: string) AI Technique from which the assets will be retrieved.
Parameter | **k**: (type: integer) the maximum number of elements retrieved.
Parameter | **to_dict**: (type: boolean) whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | (type: dictionary list or py2neo.Node list) An ordered list of assets (dictionaries{score, instance}).

## Contact points

- miquel.buxons@upc.edu
- javier.vazquez-salceda@upc.edu

