import pandas as pd
import json
import urllib.request
import os
import pickle
from py2neo import Graph
from py2neo.data import Relationship

NON_INSTANTIABLE_CLASS = ['Agent', 'AI Asset', 'AI Technique', 'AI Artifact', 'Country', 'Language', 'Container',
                          'Organisation Type', 'Education level', 'Education type', 'Skill', 'Pace', 'Distribution']
EXTRA_CLASSES = ['Philosophy of AI', 'Cross cutting concepts', 'Organization', 'organización', 'Philosophy of AI',
                 'AI Ethics']
FORBIDDEN_CLAUSES = ['CREATE', 'DELETE', 'SET', 'REMOVE', 'MERGE', 'FOREACH', 'LOAD CSV']
swaiPrefix = "swai__"
PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/developer/serverhosted-version/swai_ams.json"


class NeoDriver:
    def __init__(self, uri_l, user_n, password_n):
        # initialize the connection to the Neo4j database
        self.graph = Graph(uri_l, auth=(user_n, password_n))
        # Import fix data-structures from the static file
        self.uri, self.class_owl, self.properties, self.inverses, self.technique_hierarchy, self.business_hierarchy, \
        self.no_instance_class, self.extra_class, self.forbidden_clauses = self.read_var_file()

    # INIT RELATED METHODS #
    @staticmethod
    def read_var_file(path=None):
        """
        This method load all the fix data-structures of the AMS from a json file.
        """
        # if path is not specified, set the standard one
        if path is None:
            path = PATH_JSON
        # load all the static data-structures from the file
        with urllib.request.urlopen(path) as url:
            res = json.load(url)
        return res['uri'], pd.DataFrame.from_dict(res['class_owl'], orient='index'), \
               pd.DataFrame.from_dict(res['properties'], orient='index'), res['inverses'], res['technique_hierarchy'], \
               res['business_hierarchy'], res['no_in_classes'], res['extra_classes'], res['forbidden_clauses']

    def hard_db_reset(self):
        """
        Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI
        ontology update. This function automatically stores all the inserted instances in a backup file, restarts the
        AMS, imports the latest StairwAI ontology available, makes the changes to the swai_ams.json file and finally
        incorporates the instances available in the backup. WARNING: currently, an AMS reset will change all the node
        identifiers!
        """
        # delete all the nodes and relations of the database
        self.graph.run(
            "CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000, iterateList:true})")
        self.graph.run("CALL n10s.nsprefixes.removeAll();")
        self.graph.run("CALL apoc. schema. assert({}, {});")
        # initialise again the database, loading the ontologies
        self.init_db()
        # modify the file with the static data
        self.uri, self.class_owl, self.properties, self.inverses, self.technique_hierarchy, self.business_hierarchy = self.var_file_init(
            mod_file=True)
        # insert new internal database identifiers to all the instances already created (usually the ones specified in the ontologies)
        for individual in self.graph.nodes.match('owl__NamedIndividual').all():
            individual['swai__identifier'] = individual.identity
            self.graph.push(individual)
        print("Reset successfully done")

    def var_file_init(self, mod_file=True):
        """
        This method creates from scratch the model data-structures of the AMS using the StairwAI Ontology structure
        imported. Finally, store it in a JSON file.
        :param mod_file: whether the modification in the fixed data structures will be written in the JSON file.
        """
        uri = str.split(self.graph.nodes.match("owl__Class", rdfs__label="AI Asset").first()["uri"], sep='#')[0] + "#"
        # get all the ontology classes avoiding repetitions and None values (avoiding classes that have not ontologyNam)
        class_owl = list(set([(dtype["rdfs__label"], dtype[swaiPrefix + 'ontologyName']) for dtype in
                              self.graph.nodes.match("owl__Class").all()]))
        class_owl = [i for i in class_owl if i[0] and i[0] not in ['Organization', 'organización'] and i[1]]
        class_owl = pd.DataFrame(class_owl, columns=["label", "db_name"])
        # get classes from AI Techniques hierarchy
        technique_infer_class = self.inference_subclasses(['AI Technique'], class_owl)
        # get classes from Business categories
        bc_infer_class = self.inference_subclasses(['Business categories'], class_owl)
        # extract the properties with all its elements
        properties = self.__extract_properties(class_owl)
        # create the instanceOf relations necessary
        self.create_instance_of(class_owl)
        # inverse of list
        inverses = self.get_inverse_dict()
        # if the static file that contains all the fix database data has to be modified, modify it
        if mod_file:
            dictionary = {'uri': uri, 'class_owl': class_owl.to_dict('index'), 'no_in_classes': NON_INSTANTIABLE_CLASS,
                          'properties': properties.to_dict('index'), 'extra_classes': EXTRA_CLASSES,
                          'inverses': inverses, 'technique_hierarchy': technique_infer_class,
                          'business_hierarchy': bc_infer_class, 'forbidden_clauses': FORBIDDEN_CLAUSES}
            with open('../swai_ams.json', "w") as outfile:
                json.dump(dictionary, outfile)
        return uri, class_owl, properties, inverses, technique_infer_class, bc_infer_class

    def create_backup(self):
        """
        This method creates a StairwAI AMS back-up, with the information extracted from the nodes and relations it
        creates two pickle files where these data are stored. Use 'read_backup()' function in order to store again the
        files information to the database.
        """
        # extract instantiable class ontology names
        insta_labels = self.get_instantiable_classes(only_labels=True,only_instantiable=True)
        insta_onto = [self.class_owl.loc[self.class_owl['label'] == label].db_name.values[0] for label in insta_labels]
        # get the non instantiable class label
        nn_insta = [elem for elem in self.get_classes() if elem not in insta_labels]
        non_insta_list = []
        for label in nn_insta:
            db_name = self.class_owl.loc[self.class_owl['label'] == label].db_name.values[0]
            for node in self.graph.nodes.match("swai__"+db_name).all():
                non_insta_list.append({'name':node['rdfs__label'], 'class': label, 'old_id': node['swai__identifier']})
            for node in self.graph.nodes.match("aitec__"+db_name).all():
                non_insta_list.append({'name':node['rdfs__label'], 'class': label, 'old_id': node['swai__identifier']})
            for node in self.graph.nodes.match("aicat__"+db_name).all():
                non_insta_list.append({'name':node['rdfs__label'], 'class': label, 'old_id': node['swai__identifier']})
        nodes = []
        relations = []
        inv_buffer = {}
        for db_name in insta_onto:
            for node in self.graph.nodes.match("swai__"+db_name).all():
                # node information
                node_dict = {}
                for key, value in self.get_node_data_to_dict(node).items():
                    if key == 'uri':
                        # do not safe uri due to will be automatically generated
                        continue
                    if key == 'Class':
                        # class
                        node_dict[key] = value
                    if len(key.split("__")) == 2:
                        # Data properties
                        node_dict[key.split("__")[1]] = value
                nodes.append(node_dict)
                # relations
                for r in self.graph.match(nodes=(node,)).all():
                    if str(r.__class__.__name__) == 'owl__instanceOf':
                        continue
                    elif str(r.__class__.__name__) in ['swai__score', 'swai__scoredIn']:
                        # if the inverse is already stored, do not store
                        if str(r.start_node['swai__identifier']) +'-'+str(r.end_node['swai__identifier']) in inv_buffer:
                            continue
                        # store the relation
                        relations.append({'relation': str(r.__class__.__name__).split("__")[-1],
                                          'start_node': r.start_node['swai__identifier'],
                                          'end_node': r.end_node['swai__identifier'],
                                          'score':r['score']})
                        # if inverse exist prepare information in the buffer to not store the inverse (avoid memory overload)
                        if self.inverses[str(r.__class__.__name__).split("__")[-1]] is not None:
                            inv_buffer[str(r.end_node['swai__identifier']) +'-'+str(r.start_node['swai__identifier'])] = 'done'
                    else:
                        # if the inverse is already stored, do not store
                        if str(r.start_node['swai__identifier']) +'-'+str(r.end_node['swai__identifier']) in inv_buffer:
                            continue
                        # store the relation
                        relations.append({'relation': str(r.__class__.__name__).split("__")[-1],
                                          'start_node': r.start_node['swai__identifier'],
                                          'end_node': r.end_node['swai__identifier']})
                        # if inverse exist prepare information in the buffer to not store the inverse (avoid memory overload)
                        if self.inverses[str(r.__class__.__name__).split("__")[-1]] is not None:
                            inv_buffer[str(r.end_node['swai__identifier']) +'-'+str(r.start_node['swai__identifier'])] = 'done'
        # store the nodes extracted from the database
        with open('nodes_backup.pickle', 'wb') as handle:
            pickle.dump(nodes, handle, protocol=pickle.HIGHEST_PROTOCOL)
        # store the relations extracted from the database
        with open('relations_backup.pickle', 'wb') as handle:
            pickle.dump(relations, handle, protocol=pickle.HIGHEST_PROTOCOL)
            # store the relations extracted from the database
        with open('non_instantiable_backup.pickle', 'wb') as handle:
            pickle.dump(non_insta_list, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def read_backup():
        """
        This method read the pickle files with the AMS instances back-up generated by the method create_backup(), and
        load the instances in the AMS. Additionally, in deletes the pickle files.
        """
        # read pickle files stored information
        with open('nodes_backup.pickle', 'rb') as handle:
            nodes = pickle.load(handle)
        with open('relations_backup.pickle', 'rb') as handle:
            relations = pickle.load(handle)
        with open('non_instantiable_backup.pickle', 'rb') as handle:
            non_insta = pickle.load(handle)
        # delete the pickle files after read them
        os.remove('nodes_backup.pickle')
        os.remove('relations_backup.pickle')
        os.remove('non_instantiable_backup.pickle')
        return nodes, relations, non_insta

    def get_inverse_dict(self):
        """
        This method is used to generate the dictionary where the inverse relations are stored, obtaining a mapping
        between inverse relations extracted from the ontology imported.
        :return: a dictionary in which the inverse relation is obtained based on key-value relation.
        """
        # get all the object properties of the ontology
        res_dict = {}
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the database name of the property
            db_name = node[swaiPrefix + "ontologyName"]
            # find the db_name of the inverse relation
            inverse_rel = self.graph.match((node,), r_type="owl__inverseOf").first()
            inverse_db_name = inverse_rel.end_node[swaiPrefix + "ontologyName"] if inverse_rel is not None else None
            # if it does not have an inverse, set the value to none
            if inverse_db_name is None and db_name not in list(res_dict.keys()):
                res_dict[db_name] = None
            # if the relation has an inverse
            if inverse_db_name is not None:
                res_dict[db_name] = inverse_db_name
                res_dict[inverse_db_name] = db_name
        return res_dict

    def __extract_properties(self, class_owl):
        """
        This function extract the main characteristics of all the current properties (data or object properties), and
        store them in a pandas dataframe. Characteristics such as: label, DB name, domain, range, etc.
        :return: it returns the generated dataframe.
        """
        # get all the data properties of the ontology
        data = []
        for node in self.graph.nodes.match("owl__DatatypeProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node[swaiPrefix + "ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': None, 'type': 'DatatypeProperty'})

        # get all the object properties of the ontology
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node[swaiPrefix + "ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            # find the labels of all the range classes
            neo_range = []
            range_rel = self.graph.match((node,), r_type="rdfs__range").first()
            range_node = range_rel.end_node if range_rel is not None else None
            if range_node is not None and range_node["rdfs__label"] is None and range_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                neo_range += self.__recursive_search(
                    self.graph.match((range_node,), r_type="owl__unionOf").first().end_node, [])
            elif range_node is not None and range_node["rdfs__label"] is not None:
                neo_range.append(range_node["rdfs__label"])
            # inference the sub-range
            neo_range = self.inference_subclasses(neo_range, class_owl)
            neo_range = [r for r in neo_range if r not in ['Organization', 'organización']]
            # store the data of the node in the list of dictionaries
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': neo_range, 'type': 'ObjectProperty'})
        return pd.DataFrame(data, columns=["label", "db_name", "domain", "range", "type"])

    def inference_subclasses(self, classes, class_owl):
        """
        This method is used to do a recursive search through the ontology hierarchy with the aim of finding the children
        classes of each class in a set of classes.
        :param classes: 1D array of classes in the ontology hierarchy.
        :param class_owl: data structure generated during the JSON file creation.
        :return: a set of classes composed by the original classes and the new ones.
        """
        if len(classes) > 0:
            for class_name in classes:
                name = self.graph.run(
                    "MATCH (n:owl__Class {rdfs__label: $topic_name})<-[r:rdfs__subClassOf*]-(child:owl__Class)"
                    "RETURN child.rdfs__label", topic_name=str(class_name))
                if name is not None:
                    classes += list(name.to_ndarray().ravel())
            classes = list(set(classes))
        else:
            classes = class_owl.copy()['label'].tolist()
        return classes

    def __recursive_search(self, inter_node, labels):
        """
        This function is the recursive part of the characteristics' extraction (range and domain) from all the properties
        :param inter_node: node from which we will analise the connections.
        :param labels: the extracted labels of the classes found.
        :return: the labels
        """
        # find the desired connected nodes
        rel_eval = self.graph.match((inter_node,), r_type="rdf__first").first()
        # if empty, return empty list
        if rel_eval is None:
            return []
        elif rel_eval.end_node["rdfs__label"] is not None:
            labels += [rel_eval.end_node["rdfs__label"]]
        # recursive call searching for more categories
        labels += self.__recursive_search(self.graph.match((inter_node,), r_type="rdf__rest").first().end_node, labels)
        return list(set(labels))

    def create_instance_of(self, class_owl):
        """
        This function create the instanceOf relations that the current Neo4j importing function is not capable to do.
        :param class_owl: data structure generated during the JSON file creation.
        """
        classes = list(class_owl["label"])
        for cls in classes:
            # choose the prefix required
            if cls == "Country":
                prx = 'cry__'
            elif cls in self.business_hierarchy:
                prx = 'aicat__'
            elif cls in self.technique_hierarchy:
                prx = 'aitec__'
            else:
                prx = swaiPrefix
            # from label to db_name
            cls = self.class_owl.loc[self.class_owl["label"] == cls].values[0][1]
            # get the correct Class node for the relation
            end_nodes = self.graph.nodes.match("owl__Class", swai__ontologyName=cls).all()
            end_node = None
            # quit external nodes (Equivalent classes)
            if len(end_nodes) == 1:
                end_node = end_nodes[0]
            else:
                for end in end_nodes:
                    if "swai/spec" in end["uri"] or 'stairwai' in end["uri"]:
                        end_node = end
            # generate the relations
            for start_node in self.graph.nodes.match(prx + cls).all():
                rel = Relationship(start_node, "owl__instanceOf", end_node)
                # if it doesn't exist, add the relationship to the DB
                if rel not in self.graph.match((start_node,), r_type="owl__instanceOf").all():
                    self.graph.create(rel)

    def init_db(self):
        """
        This function initialize a StairwAI AMS new instance running the importing ontology commands and setting the
        prefixes required.
        """
        # initialise the database using the commands to set the prefixes and loading the ontologies
        print("Re-initialising the Neo4j database...")
        self.graph.run("CALL n10s.graphconfig.init();")
        self.graph.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
        self.graph.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.ttl#');")
        self.graph.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.ttl#');")
        self.graph.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#');")
        self.graph.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
        self.graph.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
        self.graph.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.ttl', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.ttl', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl', 'RDF/XML');")

    def class_4_instance(self, neo_id: int):
        """
        This function returns the class of a specific instance using its identifier.
        :param neo_id: the id associated to a specific instance.
        :return The class label from the class which the instance belongs to.
        """
        # get the node using its id
        start_node = self.query_node_by_id(neo_id=neo_id)
        # if it does not exist, raise an exception
        if start_node is None:
            raise ValueError("There is not a node in the AMS with {0} as ID".format(str(neo_id)))
        # return the class label (database version)
        return self.graph.match((start_node,), r_type='owl__instanceOf').first().end_node['rdfs__label']

    # GETTERS #
    def get_classes(self, with_descriptions=False):
        """
        Getter of the ontology defined classes, it returns all the defined owl__Class in the DB including
        non-instantiable and extra classes. Do not use to recognize possible instantiable classes. Instead, use the
        'get_instantiable_classes()' method.
        :param with_descriptions: Returns the instances with its descriptions if True
        :return: a list of the defined classes.
        """
        if with_descriptions:
            return {label: self.graph.nodes.match('owl__Class', rdfs__label=label).first()['rdfs__comment'] for label in
                    list(self.class_owl["label"].values)}
        return list(self.class_owl["label"].values)

    def get_class_id_desc(self, class_label):
        node = self.graph.nodes.match('owl__Class', rdfs__label=class_label).first()
        return node['swai__identifier'], node['rdfs__comment']
    
    # def get_aiexpert_id_desc_score(self, expert_label):
    #     """
    #     This query returns the ID and description of an expert, along with its scores.
    #     :param expert_label: string that indicates the name of the expert we are interested in
    #     :return: a dictionary containing the node ID, the description, and the scores as a 
    #     list of dictionaries with aitechnique and corresponding score. 
    #     """
    #     # Extract id and description
    #     node = self.graph.nodes.match('swai__AIExpert', rdfs__label=expert_label).first()
    #     node_id = node['swai__identifier']
    #     descr = node['swai__shortdesc']
    #     uuid_identifier = node['swai__uuidIdentifier']
    #     # Extract scores
    #     node_dict = self.query_node_by_id(neo_id=node_id, to_dict=True)
    #     ai_technique_list = [v.split('__')[0] for k, v in node_dict.items() if 'scoredIn' in k]
    #     ai_technique_dict = {label: self.graph.nodes.match('owl__Class', rdfs__label=label).first()['swai__identifier'] for label in ai_technique_list}
    #     scores = {ai_technique_dict[v.split('__')[0]] :float(v.split('__')[1]) for k, v in node_dict.items() if 'scoredIn' in k}
    #     return {'id':node_id, 'description':descr, 'scores':scores, 'uuid_identifier': uuid_identifier}

    def get_instantiable_classes(self, only_instantiable=False, only_labels=False):
        """
        Getter of the ontology instantiable classes with its definitions.
        :param only_instantiable: boolean that indicates if the non_instantiable classes have to be taken in care.
        :param only_labels: boolean that it has to be set to True if the user only wants the instantiable class labels
        and set to False if the user wants the class definitions.
        :return: a list, or dictionary, of the available classes
        """
        if only_instantiable:
            labels = [ele for ele in self.get_classes() if ele not in self.no_instance_class \
                      and ele not in self.extra_class \
                      and ele not in self.technique_hierarchy and ele not in self.business_hierarchy \
                      and ele not in self.inference_subclasses(['AI Ethics'], self.class_owl) \
                      and ele not in self.inference_subclasses(['Philosophy of AI'], self.class_owl)]
        else:
            labels = [ele for ele in self.get_classes() if ele not in self.extra_class \
                      and ele not in self.technique_hierarchy and ele not in self.business_hierarchy \
                      and ele not in self.inference_subclasses(['AI Ethics'], self.class_owl) \
                      and ele not in self.inference_subclasses(['Philosophy of AI'], self.class_owl)]
        if not only_labels:
            return {label: self.graph.nodes.match('owl__Class', rdfs__label=label).first()['rdfs__comment'] for label in
                    labels}
        else:
            return labels

    def get_node_data_to_dict(self, node):
        """
        This query returns the node data as a dictionary.
        :param node: py2neo.Node instance.
        :return: Node data as a dictionary, if node = None, returns None.
        """
        if node is not None:
            # Translate directly the node attributes
            res = dict(node)
            # Prepare data structures for some special relations
            scores = {}
            sc_rel = ''
            # scores_id = {}
            # Translate all relations
            for r in self.graph.match(nodes=(node,)).all():
                if str(r.__class__.__name__) in ['swai__score', 'swai__scoredIn']:
                    scores[r.end_node["rdfs__label"]] = r['score']
                    sc_rel = str(r.__class__.__name__)
                    # scores_id[r.end_node["swai__identifier"]] = r['score']
                elif str(r.__class__.__name__) == 'owl__instanceOf':
                    res['owl__instanceOf'] = r.end_node["rdfs__label"]
                else:
                    field_name = str(r.__class__.__name__)
                    # if country the name is in the property 'foaf__name'
                    if r.end_node["rdfs__label"] is None:
                        r.end_node["rdfs__label"] = r.end_node["foaf__name"]
                    # if new label, insert
                    if field_name not in res.keys():
                        res[field_name] = r.end_node["rdfs__label"]
                    else:
                        if isinstance(res[field_name], list):
                            res[field_name].append(r.end_node["rdfs__label"])
                        else:
                            aux = res[field_name]
                            res[field_name] = [aux, r.end_node["rdfs__label"]]
            # Store the special relations if there are any, the label can be 'swai__score' or 'swai__scoredIn'
            if len(scores.keys()) > 0:
                res[sc_rel] = scores
            res2 = {}
            for db_k in res.keys():
                # extract database internal properties from the result
                if db_k == 'uri' or str(db_k).split('__')[1] in ['label', 'ontologyName', 'comment', 'subClassOf']:
                    pass
                elif str(db_k).split('__')[1] == 'instanceOf':
                    res2[db_k] = res[db_k]
                else:
                    res2['swai__' +
                         self.properties.loc[self.properties["db_name"] == str(db_k).split('__')[1]]['label'].values[
                             0].replace(' ', '_')] = res[db_k]
            return res2
        return None

    def get_class_properties(self, name_class: str):
        """
        This method returns a dictionary with the properties, and its definitions, of a specific type of class.
        :param name_class: the name (label) of the class from which to extract the properties.
        :return: A dictionary with the properties available for a specific class and its requirements.
        """
        # set the result array
        properties = {'Attributes': {'def': '', 'properties': []}, 'Relations': {'def': '', 'properties': []}}
        # property definitions insertion
        properties['Attributes'][
            'def'] = "Inherent features of each instance. Only one attribute of each type can exist per instance and the data type of each attribute is defined in its 'range' parameter."
        properties['Relations'][
            'def'] = "Connections between already existing instances in the database. More than one relation of each type can exist per instance and the 'range' parameter of each relation defines the class which the relation is used to connect with."
        for idx, proper in self.properties.iterrows():
            # Data properties
            if proper['type'] == 'DatatypeProperty' and name_class in proper['domain']:
                rel_node = self.graph.nodes.match('owl__DatatypeProperty', rdfs__label=proper['label']).first()
                rng = self.graph.match((rel_node,), r_type='rdfs__range').first().nodes[1]['uri'].split('#')[1]
                properties['Attributes']['properties'].append(
                    {'property': 'swai__'+proper['label'].replace(" ", "_"), 'range': rng, 'def': rel_node['rdfs__comment']})
            # Object properties
            elif proper['type'] == 'ObjectProperty' and name_class in proper['domain']:
                rel_node = self.graph.nodes.match('owl__ObjectProperty', rdfs__label=proper['label']).first()
                rel = self.graph.match((rel_node,), r_type='rdfs__range').first()
                # avoid hasPart and isPartOf general relations
                if rel is not None:
                    properties['Relations']['properties'].append(
                        {'property': 'swai__'+proper['label'].replace(" ", "_"), 'range': proper['range'],
                         'def': rel_node['rdfs__comment']})
        return properties

    def get_attributes(self, with_conditions: bool):
        """
        Getter of the ontology defined attributes.
        :param with_conditions: whether the method must return the domain of each property.
        :return: If with_conditions is True, a pandas data frame with the properties and its domain. Otherwise, it will
        return the list of available attributes.
        """
        # get the list of all the attributes in the database
        prop = self.properties.loc[self.properties["type"] == 'DatatypeProperty'].reset_index()
        # returning a list of names if with_conditions false and returning more properties if it is true
        return prop[["label", "domain"]] if with_conditions else list(prop['label'].values)

    def get_relations(self, with_conditions: bool):
        """
        Getter of the ontology defined relations.
        :param with_conditions: whether the method must return the domain/range of relation.
        :return: If with_conditions is True, a pandas' data frame with the relations and its domain/range. Otherwise, it
        will return the list of available relations.
        """
        # get the list of all the relations in the database
        rel = self.properties.loc[self.properties["type"] == 'ObjectProperty'].reset_index()
        # returning a list of names if with_conditions false and returning more properties if it is true
        return rel[["label", "domain", "range"]] if with_conditions else list(rel['label'].values)

    def get_instances_by_class(self, class_label: str, with_description: bool = False):
        """
        Get all the instances defined by class.
        :param class_label: the class from which the instances are extracted.
        :param with_description: returns the description of each label
        :return: list of the available instances.
        """
        # for each class type possible return the instances of the class label specified as a parameter
        if class_label == "Country":
            return [node["foaf__name"] for node in
                    self.graph.nodes.match('cry__' + class_label).all() if node["foaf__name"]]
        elif class_label == 'BusinessCategories':
            return self.business_hierarchy
        elif class_label == 'AITechnique':
            return self.technique_hierarchy
        else:
            return [node["rdfs__label"] if not with_description else {'label':node["rdfs__label"], 'desc':node['rdfs__comment']} for node in self.graph.nodes.match(swaiPrefix + class_label).all()]

    def get_id_from_label_relation(self, label, relation):
        """
        This method returns the id of a node given its label, if it exists in the range of a specific relation.
        :param label: name of the instance.
        :param relation: relation in which range there have to be the instance with the name specified in the label.
        :return: The id of the instance searched, if exists.
        """
        # get the classes in the range of rel
        range_cls = self.properties.loc[self.properties["label"] == relation]["range"].values[0]
        # for each class range search if the instance exists
        res = None
        for rg_cls in range_cls:
            node = self.query_node_by_name_class(db_name=label, db_class=rg_cls)
            if node is not None:
                res = node['swai__identifier']
        return res

    # INSERTERS, MODIFIERS AND DELETORS #
    def insert_node(self, node_name, node_class):
        """
        Method to insert new nodes in the AMS.
        :param node_name: name of the concept that you want to insert
        :param node_class: class of the concept that you want to insert
        :return the node of the instance generated.
        """
        # if the class is not an instantiable class, raise an exception
        if node_class not in self.get_instantiable_classes(only_instantiable=True, only_labels=True):
            raise Exception("{0} class instances cannot be instantiated or deleted, only instantiable classes can.".format(node_class))
        # get the database name of the class specified as a parameter
        db_class_node = self.class_owl.loc[self.class_owl["label"] == node_class].values[0][1]
        # Check if the node already exists, if not, insert it.
        if self.graph.nodes.match(swaiPrefix + db_class_node, swai__name=node_name).first() is None:
            self.graph.run(
                "CREATE (n:Resource:owl__NamedIndividual:" + swaiPrefix + db_class_node + " {" + swaiPrefix +
                "name:$name, rdfs__label:$name, uri:$uri})",
                {"name": node_name, "uri": self.uri + db_class_node + '%' + node_name.replace(' ', '')})
            # set the relation "instance of" between the new node and its class
            db_name = self.class_owl.loc[self.class_owl["label"] == node_class].values[0][1]
            start_node = self.graph.nodes.match(swaiPrefix + db_name, swai__name=node_name).first()
            end_nodes = self.graph.nodes.match("owl__Class", rdfs__label=node_class).all()
            end_node = None
            for end in end_nodes:
                if "swai" in end["uri"]:
                    end_node = end
            self.graph.create(Relationship(start_node, "owl__instanceOf", end_node))
            return start_node
        else:
            raise ValueError(
                'Insertion cancelled, the following instance is already in the Database: ' + node_name)

    def delete_node(self, neo_id):
        """
        Method to delete nodes in the AMS.
        :param neo_id: identifier of the instance which you want to modify.
        """
        # get the node by its id, if it does not exist, raise and exception
        node = self.query_node_by_id(neo_id=int(neo_id))
        if node is None:
            raise ValueError(
                'Deletion cancelled, there is not an instance in the Database with the ID:{0}'.format(str(neo_id)))
        # extract the class of the node
        node_class = self.class_4_instance(neo_id=neo_id)
        node_name = node['foaf__name'] if node_class == 'Country' else node['rdfs__label']
        # if the node specified it belongs to an instantiable class, continue
        if node_class not in self.get_instantiable_classes(only_instantiable=True, only_labels=True):
            raise Exception("{0} class instances cannot be instantiated or deleted, only instantiable classes can.".format(node_class))
        # get the database name of the node class and delete the node
        db_name_node = self.class_owl.loc[self.class_owl["label"] == node_class].values[0][1]
        self.graph.run("MATCH (n:" + swaiPrefix + db_name_node + " {" + swaiPrefix + "name:$name}) DETACH DELETE n",
                       {"name": node_name})

    def insert_property(self, neo_id, relation: str, value=None, end_node_id: int = None):
        """
        Insert a new property in the AMS. This method is able to handle the insertion of new relations between nodes,
        and the node property insertion/modification.
        :param neo_id: Identifier of the instance (node) from which you want to set the new relation or attribute.
        :param relation: relation/property type. Must be a relation defined in the ontology.
        :param value: The value that you want to set (only used in the case of attributes and score/scored_in relations).
        :param end_node_id: identifier of the node to which you want to connect (only used in relation insertion).
        """
        # get the start node and check if exist
        start_node = self.query_node_by_id(neo_id=neo_id)
        if start_node is None:
            raise ValueError(
                'Property deletion cancelled, there is not an instance in the Database with the ID:{0}'.format(
                    str(neo_id)))
        start_node_class = self.class_4_instance(neo_id=neo_id)
        start_node_name = start_node['foaf__name'] if start_node_class == 'Country' else start_node['rdfs__label']
        # check if the relation type exist
        if relation not in self.properties["label"].values:
            raise ValueError("The relation type: " + relation + " does not exist in the AMS")
        # Check if it has the class of start_node in the domain
        df_row = self.properties.loc[self.properties["label"] == relation]
        if start_node_class not in df_row["domain"].to_list()[0]:
            raise ValueError(start_node_class + " class is not in the domain of the relation " + relation)
        # if Object property (relation)
        if df_row["type"].values == 'ObjectProperty':
            # check that end_node already exists and get the correct type
            end_node = self.query_node_by_id(neo_id=end_node_id)
            end_node_class = self.class_4_instance(neo_id=end_node_id)
            end_node_name = end_node['foaf__name'] if end_node_class == 'Country' else end_node['rdfs__label']
            # if relation is score or scored in
            if relation in ['score', 'scored in']:
                if value:
                    rel = Relationship(start_node, swaiPrefix + df_row["db_name"].values[0], end_node,
                                       score=float(value))
                else:
                    raise Exception(
                        "The relation {0} from {1} to {2} requires a score".format(relation, start_node_name,
                                                                                   end_node_name))
            else:
                rel = Relationship(start_node, swaiPrefix + df_row["db_name"].values[0], end_node)
            # if we want to support specific N to 1 relation, must be checked here.!!
            # add the relationship to the DB
            self.graph.create(rel)
            # insert inverse relation, if exists
            if self.inverses[df_row["db_name"].values[0]]:
                # check if the relation already exists
                inv_rel_db = self.inverses[df_row["db_name"].values[0]]
                inv_rel_name = self.properties.loc[self.properties["db_name"] == inv_rel_db]['label'].values[0]
                # create the new relation
                if relation in ['score', 'scored in']:
                    if value:
                        inv_rel = Relationship(end_node, swaiPrefix + self.inverses[df_row["db_name"].values[0]],
                                               start_node, score=float(value))
                    else:
                        raise Exception(
                            "The relation {0} from {1} to {2} requires a score".format(relation, start_node_name,
                                                                                       end_node_name))
                else:
                    inv_rel = Relationship(end_node, swaiPrefix + self.inverses[df_row["db_name"].values[0]],
                                           start_node)
                # add the relationship to the DB
                self.graph.create(inv_rel)
        # if DataType properties (attributes)
        else:
            # add the new property into the node, or update in with the new value if it already exists
            if value is not None:
                start_node[swaiPrefix + df_row["db_name"].values[0]] = value
                self.graph.push(start_node)
            else:
                raise Exception("The attribute {0} from {1} requires a value".format(relation, start_node_name))

    def delete_property(self, neo_id, relation: str, end_node_id: int = None, error_if_not_existing=True):
        """
        Delete a new property in the AMS. This method is able to handle the deletion of existing relations between
        nodes, and node properties deletion
        :param neo_id: Identifier of the instance (node) from which you want to delete a relation or attribute.
        :param relation: relation/property type. Must be a relation defined in the ontology.
        :param end_node_id: identifier of the node to which you want to connect (only used in relation insertion).
        """
        start_node = self.query_node_by_id(neo_id=neo_id)
        if start_node is None:
            raise ValueError(
                'Property deletion cancelled, there is not an instance in the Database with the ID:{0}'.format(
                    str(neo_id)))
        start_node_class = self.class_4_instance(neo_id=neo_id)
        # check if the relation type exist
        if error_if_not_existing and relation not in self.properties["label"].values:
            raise ValueError("The relation type: " + relation + " does not exist in the AMS")
        # Check if it has the class of start_node in the domain
        df_row = self.properties.loc[self.properties["label"] == relation]
        if start_node_class not in df_row["domain"].to_list()[0]:
            raise ValueError(start_node_class + " class is not in the domain of the relation " + relation)
        # if Object property (relation)
        if df_row["type"].values == 'ObjectProperty':
            # check that end_node already exists and get the correct type
            end_node = self.query_node_by_id(neo_id=end_node_id)
            if end_node is None:
                raise ValueError(
                    'Relation insertion cancelled, there is not an instance in the Database with the ID:{0}'.format(
                        str(neo_id)))
            end_node_class = self.class_4_instance(neo_id=end_node_id)
            end_node_name = end_node['foaf__name'] if end_node_class == 'Country' else end_node['rdfs__label']
            # Check if it has the class of end_node is in the range
            if end_node_class not in df_row["range"].values[0]:
                raise ValueError(str(end_node_name) + " is not in the range of the relation proposed. ")
            if error_if_not_existing and not self.query_if_relation_exists(neo_id, df_row["label"].values[0], end_node_id):
                raise ValueError("The relation can't be deleted because it is not in the AMS")
            # delete the relationship in the AMS
            self.graph.run(
                'MATCH(a:owl__NamedIndividual)-[r:{0}]->(b:owl__NamedIndividual) WHERE ID(a) = $a AND ID(b) = $b DELETE r'.format(
                    'swai__' + df_row["db_name"].values[0]), {'a': neo_id, 'b': end_node_id})
            # delete inverse relation, if exists
            if self.inverses[df_row["db_name"].values[0]]:
                inv_rel_db = self.inverses[df_row["db_name"].values[0]]
                inv_rel_name = self.properties.loc[self.properties["db_name"] == inv_rel_db]['label'].values[0]
                if error_if_not_existing and not self.query_if_relation_exists(end_node_id, inv_rel_name, neo_id):
                    raise ValueError("The relation can't be deleted because it is not in the AMS")
                # delete the relationship in the AMS
                self.graph.run(
                    'MATCH(a:owl__NamedIndividual)-[r:{0}]->(b:owl__NamedIndividual) WHERE ID(a) = $a AND ID(b) = $b DELETE r'.format(
                        'swai__' + inv_rel_db), {'a': end_node_id, 'b': neo_id})
        # if DataType properties (attributes)
        else:
            # add the new property into the node, or update in with the new value if it already exists
            if relation != "name":
                del (start_node[swaiPrefix + df_row["db_name"].values[0]])
                self.graph.push(start_node)

    # QUERIES #
    def free_query(self, query: str):
        """
        This query allows a personalized query construction.
        :param query: string with the Cypher code command (query) that is going to be used.
        :return: The result of the query, without post-processing.
        """
        # control if the query has any of the forbidden clauses
        if any(ele in query.upper() for ele in self.forbidden_clauses):
            raise Exception("The query proposed contains at least one of the following forbidden clauses: {0}".format(
                self.forbidden_clauses))
        # if all correct, run the query
        return self.graph.query(query)

    def query_exist_instance(self, neo_id: int):
        """
        This query returns true if a specific instance exists in the database.
        :param neo_id: ID of the instance searched.
        :return: True if the instance identified by id exists in the AMS, False otherwise.
        """
        # if query_node_by_id returns a node, return True. If not, False
        if self.query_node_by_id(neo_id):
            return True
        return False

    def query_if_relation_exists(self, start_node_id:int, relation:str, end_node_id:int):
        """
        This query returns True if a specific relation exists in the AMS, or returns false if it does not exist
        :param start_node_id: identifier of the starting instance in the relation (all the relations in the AMS are
        directed).
        :param relation: relation to be searched.
        :param end_node_id: identifier of the ending instance in the relation (all the relations in the AMS are
        directed).
        :return: A boolean with the result of the query.
        """
        # search if exist any relation between the two instances specified in the parameters
        res = self.graph.run("MATCH (s)-[r]->(e) WHERE ID(s) = $start AND ID(e) = $end RETURN r", {'start':start_node_id, 'end':end_node_id}).data()
        # if no relation exist between them, return False.
        if len(res) == 0:
            return False
        # get the database name of the relation specified in the parameters
        df_row = self.properties.loc[self.properties["label"] == relation]
        db_rel = df_row["db_name"].values[0]
        # if the database name matches with any of the relations extracted before, return True. If not, return False
        for rel in res:
            if type(rel['r']).__name__.split('__')[1] == db_rel:
                return True
        return False

    def query_node_by_name_class(self, db_name: str, db_class: str, to_dict=False):
        """
        This query returns an instance, if it exists, based on its name and the class.
        :param db_name: name of the instance searched.
        :param db_class: class of the instance searched.
        :param to_dict: whether the result must be returned as a dictionary (True) or apy2neo.Node instance (False).
        :return: Instance data or instance node, if it exists.
        """
        # get the database name of the class label specified as a parameter
        db_name_start_node = self.class_owl.loc[self.class_owl["label"] == db_class].values[0][1]
        # for any particular case, specify the correct query to the database
        if db_name_start_node == "Country":
            node = self.graph.nodes.match('cry__' + db_name_start_node, foaf__name=db_name).first()
        elif db_name_start_node == 'BusinessCategories':
            ams_name = self.class_owl.loc[self.class_owl["label"] == db_name].values[0][1]
            node = self.graph.nodes.match('aicat__' + ams_name, rdfs__label=db_name).first()
        elif db_name_start_node == 'AITechnique':
            ams_name = self.class_owl.loc[self.class_owl["label"] == db_name].values[0][1]
            node = self.graph.nodes.match('aitec__' + ams_name, rdfs__label=db_name).first()
        else:
            node = self.graph.nodes.match(swaiPrefix + db_name_start_node, rdfs__label=db_name).first()
        # if to_dict is True, return the node as a dictionary
        if to_dict:
            return self.get_node_data_to_dict(node)
        else:
            return node
    def query_node_by_uuid(self, uuid: str, db_class:str, to_dict: bool = False):
        """
        This query returns an instance, if it exists, based on its name and the class.
        :param uuid: UUID identifier of the instance searched.
        :param db_class: class of the instance searched.
        :param to_dict: whether the result must be returned as a dictionary (True) or apy2neo.Node instance (False).
        :return: Instance data or instance node, if it exists.
        """
        # change the class label to its ontology name
        db_name_start_node = self.class_owl.loc[self.class_owl["label"] == db_class].values[0][1]
        # search the node by its id
        node = self.graph.nodes.match(swaiPrefix + db_name_start_node, swai__uuidIdentifier=uuid).first()
        # if to_dict is True, return the node as a dictionary
        if to_dict:
            return self.get_node_data_to_dict(node)
        else:
            return node

    def query_node_by_name(self, name: str, to_dict=False):
        """
        This query returns an instance, if it exists, given its name. WARNING: DO NOT USE UNLESS IT IS ABSOLUTELY
        NECESSARY, very slow function with exponential time cost. Alternatively, use query_node_by_name_class that uses
        the instance class also.
        :param name: name of the instance searched.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :return: Instance data or instance node, if it exists.
        """
        # query the name of the label and compare to all the instances that exist in the database
        node = self.graph.nodes.match('owl__NamedIndividual', rdfs__label=name).first()
        # if to_dict is True, return the node as a dictionary
        if to_dict:
            return self.get_node_data_to_dict(node)
        else:
            return node

    def query_node_by_id(self, neo_id: int, to_dict=False):
        """
        This query returns the node, if it exists, given its identifier.
        :param neo_id: id of the instance searched.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :return: Instance data or instance node, if it exists.
        """
        # query a instance to the database using its identifier
        try:
            node = self.graph.run("MATCH (n) WHERE ID(n)={0} RETURN n".format(str(neo_id))).data()[0]['n']
        except Exception:
            node = None
        # if to_dict is True, return the node as a dictionary
        if to_dict:
            return self.get_node_data_to_dict(node)
        return node

    def query_instances_by_class(self, class_label: str, with_inference: bool = True, to_dict=False):
        """
        This query returns all the instances of a specific class.
        :param class_label: class name.
        :param with_inference: in the case of True value in this parameter, the query will retrieve the instances of the
        class specified as a parameter and all the instances of its subclasses also.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :return: List of the instance data or instance nodes, if they exist.
        """
        # get the database name of the class specified as a parameter
        alt_class_label = self.class_owl.loc[self.class_owl["label"] == class_label].values[0][1]
        # if with_inference is True, search all the subclasses of the one specified as a parameter
        if with_inference:
            db_classes = self.inference_subclasses([alt_class_label], self.class_owl)
        else:
            db_classes = [self.class_owl.loc[self.class_owl["label"] == alt_class_label].values[0][1]]
        # get all the instances by its class, and flatten them as a plain list of nodes
        nodes = [self.graph.nodes.match(swaiPrefix + db_class, ).all() for db_class in db_classes]
        flattered_nodes = [item for sublist in nodes for item in sublist]
        # if to_dict is True, return the node as a dictionary
        return [self.get_node_data_to_dict(node) if to_dict else node for node in flattered_nodes]

    def query_related_nodes_by_relation(self, neo_id: int, relation: str, to_dict=False):
        """
        This query returns the data of the instances related to the given identifier and relation.
        :param neo_id: id of the instance searched.
        :param relation: relation to be searched.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :return: List of the instance data, or instance nodes, if they exist.
        """
        # get the starting node
        start_node = self.query_node_by_id(neo_id=neo_id)
        # if it does not exist, return exception
        if start_node is None:
            raise ValueError(
                "There is not a node in the AMS with {0} as ID".format(str(neo_id)))
        # get the relations from the database that has as a label the relation passed as a parameter
        df_row = self.properties.loc[self.properties["label"] == relation]
        relations = self.graph.match((start_node,), r_type=swaiPrefix + df_row["db_name"].values[0]).all()
        # if any relation extracted return None
        if relations is None or len(relations) == 0:
            return None
        # if to_dict is True, return the node as a dictionary
        return [self.get_node_data_to_dict(relation.end_node) if to_dict else relation.end_node for relation in
                relations]

    def query_get_assets_by_tag(self, categories: list, limit: int, inference_cat: bool = False, to_dict: bool = False):
        """
        This query returns a list of AI Artifacts that belongs to a specific AI Technique.
        :param categories: list of AI Techniques from which to retrieve the instances.
        :param limit: limit of nodes to be retrieved by the query.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :param inference_cat: whether to infer the AI Techniques subcategories of the ones in categories parameter.
        :return: A list of dictionaries where, for each category searched, a dictionary is retrieved with the artifact
        and the category that belongs to.
        """
        #  relations of interest for this query
        interest_relations = ['applied on', 'has been applied in']
        # check that all the categories are supported by the ontology
        for cat1 in categories:
            if cat1 not in self.technique_hierarchy:
                raise Exception("All the categories passed should be AI Techniques, {0} is not one".format(str(cat1)))
        # infer sub-categories if the parameter inference_cat is True
        if inference_cat:
            categories = self.inference_subclasses(categories, self.class_owl)
        # extract the categories nodes
        node_cat = [{'node': self.graph.nodes.match(
            'aitec__' + self.class_owl.loc[self.class_owl["label"] == cat].values[0][1]).first(), 'cat': cat} for cat in
                    categories]
        # get the AI assets extracted for each category
        nodes = []
        for node in node_cat:
            aux = []
            for rel in interest_relations:
                if node['node'] is not None:
                    aux2 = aux + self.query_related_nodes_by_relation(node['node'].identity, rel, to_dict=to_dict)
                    aux = aux2
            if len(aux) > limit:
                aux = aux[:limit]
            if len(aux) > 0:
                nodes.append({'category': node['cat'], 'nodes': aux})
        # if to_dict is True, return the node as a dictionary
        return [self.get_node_data_to_dict(node) if to_dict else node for node in nodes]

    def query_get_experts_by_tag(self, categories: list, limit: int, inference_cat: bool = False,
                                 to_dict: bool = False):
        """
        This query returns a list of AI Experts that belongs to a specific AI Technique.
        :param categories: list of AI Techniques from which to retrieve the instances.
        :param limit: limit of nodes to be retrieved by the query.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :param inference_cat: whether to infer the AI Techniques subcategories of the ones in categories parameter.
        :return: A list of dictionaries where, for each category searched, a dictionary is retrieved with the expert
        and the category that belongs to.
        """
        #  relations of interest for this query
        interest_relations = ['expertised by']
        # check that all the categories are supported by the ontology
        for cat1 in categories:
            if cat1 not in self.technique_hierarchy:
                raise Exception("All the categories passed should be AI Techniques, {0} is not one".format(str(cat1)))
        # infer sub-categories if the parameter inference_cat is True
        if inference_cat:
            categories = self.inference_subclasses(categories, self.class_owl)
        # extract the categories nodes
        node_cat = [{'node': self.graph.nodes.match(
            'aitec__' + self.class_owl.loc[self.class_owl["label"] == cat].values[0][1]).first(), 'cat': cat} for cat in
                    categories]
        # get the AI assets extracted for each category
        nodes = []
        for node in node_cat:
            aux = []
            for rel in interest_relations:
                if node['node'] is not None:
                    a1 = self.query_related_nodes_by_relation(node['node'].identity, rel, to_dict=to_dict)
                    if a1 is not None:
                        aux.append(a1)
            if len(aux) > limit:
                aux = aux[:limit]
            if len(aux) > 0:
                nodes.append({'category': node['cat'], 'nodes': aux})
        # if to_dict is True, return the node as a dictionary
        return [self.get_node_data_to_dict(node) if to_dict else node for node in nodes]

    def query_topk_assets(self, category: str, k: int, to_dict: bool = False):
        """
        This query returns an ordered asset list with, at most, K elements based on the scored assets in AI Technique.
        :param category: AI Technique from which the assets will be retrieved.
        :param k: the maximum number of elements retrieved.
        :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
        :return: An ordered list of assets (dictionaries{score, instance}).
        """
        # check that the category is an AI technique in the database
        if category not in self.technique_hierarchy:
            raise Exception("The category passed should be AI Technique, {0} is not one".format(str(category)))
        # get the node from the category passed as a parameter
        cat_node = self.graph.nodes.match(
            'aitec__' + self.class_owl.loc[self.class_owl["label"] == category].values[0][1]).first()
        # extract the top K elements scored for the category
        res_rel = self.graph.match((cat_node,), r_type='swai__score').order_by('2 - (_.score + 1)').limit(k).all()
        nodes = [{'score': rel['score'], 'node': rel.end_node} for rel in res_rel]
        # if to_dict is True, return the node as a dictionary
        return [{'score': node['score'], 'node': self.get_node_data_to_dict(node['node'])} if to_dict else node for node
                in nodes]
