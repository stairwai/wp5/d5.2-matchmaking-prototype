from kr.StairwAIDriver import Driver

# init the driver
driver = Driver()

# examples of the getters available ######################
print(driver.get_classes())
print(driver.get_instantiable_classes())
print(driver.get_attributes(with_conditions=True))
print(driver.get_relations(with_conditions=False))
print(driver.get_node_data_to_dict(driver.query_node_by_id(driver.query_node_by_name_class('PIXANO', 'Library', to_dict=True)['swai__identifier'])))
[print(x['property']) for x in driver.get_class_properties('Course')['Relations']['properties']]
print("\n")
[print(x['property']) for x in driver.get_class_properties('Course')['Attributes']['properties']]
print(driver.get_business_categories())
print(driver.get_aitechniques())
print(driver.get_countries())
print(driver.get_containers())
print(driver.get_education_types())
print(driver.get_organisation_types())
print(driver.get_education_levels())
print(driver.get_distributions())
print(driver.get_paces())
print(driver.get_languages())
print(driver.get_skills())

# examples of insertions/modifications/deletions of AMS instances ######################
# AI Artifact -> Algorithm
algorithm_params = {'swai__name': 'Example',
                 'swai__short_description':"Example of description",
                 'swai__keyword': 'Example',
                 'swai__has_language':'Greek',
                 'swai__distributed_as':'SAAS',
                 'swai__wrapped_in': 'jupyter notebook',
                 'swai__applicable_to': ['Agriculture', 'Energy'],
                 'swai__apply': 'Machine learning',
                 'swai__scored_in': {'Searching':0.9, 'Machine learning':0.8},
                 'swai__license_of_distribution':"licence of the example",
                 'swai__gdpr_requirements':'GDPR requirements',
                 'swai__trustworthy_AI':'Trustworthy AI reqs'
                 }
node = driver.insert_instance(class_type='Algorithm',param_dict=algorithm_params)
driver.modify_instance(node['swai__identifier'], delete_property=False, param_dict={'swai__detailed_description':"insert a detailed description"})
print(driver.query_node_by_name_class(name="Example", ont_class="Algorithm"))
driver.delete_instance(node['swai__identifier'])

# AI expert
expert_params = {'swai__name': 'Example',
                 'swai__short_description':"Example of description",
                 'swai__keyword': 'Example',
                 'swai__has_language':'Greek',
                 'swai__has_organisation_type':'Startup company',
                 'swai__has_educational_level': 'Master',
                 'swai__resides': 'Kazakhstan',
                 'swai__experience_in': ['Agriculture', 'Energy'],
                 'swai__scored_in': {'Searching':0.9, 'Machine learning':0.8}
                 }
node = driver.insert_instance(class_type='AI expert',param_dict=expert_params)
driver.modify_instance(node['swai__identifier'], delete_property=False, param_dict={'swai__available_expert':True})
print(driver.query_node_by_name_class(name="Example", ont_class="AI expert"))
driver.delete_instance(node['swai__identifier'])

# Course
course_params = {'swai__name': 'Example',
                 'swai__short_description':"Example of description",
                 'swai__keyword': 'Example',
                 'swai__has_language':'Greek',
                 'swai__has_educational_level': 'Master',
                 'swai__imparted_in': 'Kazakhstan',
                 'swai__is_paced_as':'Full-time',
                 'swai__imparted_as': 'On-site'
                 }
node = driver.insert_instance(class_type='Course',param_dict=course_params)
print(driver.query_node_by_name_class(name="Example", ont_class="Course"))
driver.delete_instance(node['swai__identifier'])

# Node id specification
nlp_id = driver.query_node_by_name_class('Natural language processing', 'AI Technique', to_dict=True)['swai__identifier']
ex1_id = driver.query_node_by_name_class('PIXANO', 'Library', to_dict=True)['swai__identifier']
ex2_id = driver.query_node_by_name_class('PREMIN', 'Library', to_dict=True)['swai__identifier']
ex3_id = driver.query_node_by_name_class('ABELE', 'Library', to_dict=True)['swai__identifier']

# Insert/delete a score examples ######################
driver.modify_score(nlp_id, end_node_id=ex1_id, score=0.5)  # insert a score
driver.modify_score(nlp_id, end_node_id=ex2_id, score=0.7)  # insert a score
driver.modify_score(nlp_id,end_node_id=ex3_id, score=0.2)  # insert a score
# insert scores as a list of dictionaries
driver.score_list_modifier([{'start_node_id':nlp_id, 'end_node_id':ex1_id, 'score':0.5},
                            {'start_node_id':nlp_id, 'end_node_id':ex2_id, 'score':0.7},
                            {'start_node_id':nlp_id, 'end_node_id':ex3_id, 'score':0.2}])
print(driver.query_topk_assets('Natural language processing', 5, to_dict=True))  # TOPk node scores connected to NLP
driver.modify_score(nlp_id,  end_node_id=ex1_id, delete_property=True)  # delete a score
driver.modify_score(nlp_id, end_node_id=ex2_id, delete_property=True)  # delete a score
driver.modify_score(nlp_id,end_node_id=ex3_id, delete_property=True)  # delete a score

# examples of calls to the queries available ######################
print(driver.query_class_by_id(neo_id=nlp_id))
print(driver.query_node_by_id(neo_id=ex2_id, to_dict=False))
print(driver.query_node_by_name_class(name='KENN', ont_class='Library', to_dict=True))
print(driver.free_query('MATCH (n:owl__NamedIndividual) WHERE n.swai__identifier = {0} RETURN n'.format(ex1_id)))
print(driver.query_exist_instance(neo_id=ex1_id))
print(driver.query_instances_by_class('Tool', with_inference=True))
print(driver.query_related_nodes_by_relation(neo_id=ex3_id, relation='owner'))
print(driver.query_get_assets_by_tag(['Machine learning'], 5, inference_cat=False)[0]['nodes'])
print(driver.query_get_experts_by_tag(['Machine learning'], 4, inference_cat=True))

# Insert/delete a score examples ######################
# number = 100
# sum_el = 0
# for n in range(number):
#     st = time.time()
#     driver.modify_score(nlp_id, end_node_id=ex2_id, score=0.5)  # insert a score
#     ed = time.time()
#     sum_el += (ed - st)
#     if n % 10 == 0:
#         print(n)
# print('Mean execution time:', sum_el/number, 'seconds')