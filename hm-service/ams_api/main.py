import pandas as pd
import math
from kr.StairwAIDriver import Driver


def data_dict(driver_ls, row):
    data = {}
    data['owner'] = driver_ls.query_node_by_name_class(row['Developed by'], 'Organisation', to_dict=True)[
        'swai__identifier']
    if row['Distribution'] is not None and row["Class"] != 'Dataset':
        data['distributed_as'] = driver_ls.query_node_by_name_class(row['Distribution'], 'Distribution', to_dict=True)[
            'swai__identifier']

    if row['Container'] is not None and row["Class"] != 'Dataset':
        data['wrapped_in'] = driver_ls.query_node_by_name_class(row['Container'], 'Container', to_dict=True)[
            'swai__identifier']

    if row["Class"] != 'Dataset':
        data['apply'] = driver_ls.query_node_by_name_class(row['Technical category'], 'AI Technique', to_dict=True)[
            'swai__identifier']
    else:
        data['useful_for'] = driver_ls.query_node_by_name_class(row['Technical category'], 'AI Technique', to_dict=True)[
            'swai__identifier']

    if row["Class"] != 'Dataset':
        data['applicable_to'] = \
        driver_ls.query_node_by_name_class(row['Business category'], 'Business categories', to_dict=True)[
            'swai__identifier']
    else:
        data['related_to'] = \
        driver_ls.query_node_by_name_class(row['Business category'], 'Business categories', to_dict=True)[
            'swai__identifier']
    return data


def insert_ai4eu_assets(driver_l: Driver, dataset_url, categories_url):
    # AI4EU assets dataset
    dataset = pd.read_csv(dataset_url, sep=';')

    # mapping of all technical categories from AI4EU classification to CSO categories
    categories_map = pd.read_csv(categories_url, sep=';', na_values='NA')

    for index, row in dataset.iterrows():
        row = row.to_dict()
        for key, value in row.items():
            if type(value) is float and math.isnan(value): row[key] = None

        if driver_l.query_node_by_name_class(row["Title"], row["Class"]) is None:
            data = data_dict(driver_l, row)
            if row["Class"] == 'Model':
                driver_l.insert_instance(row["Title"], class_type=row["Class"],
                                         short_description=row['Brief description'],
                                         keyword=row['Related tags'],license_of_distribution=row['Licence'],
                                         description=row['Main characteristics'],
                                         detailed_description=row['Detailed description'],
                                         gdpr_requirements=row['GDPR requirements'],
                                         trustworthy_AI=row['Trustworthy AI'],
                                         url=row['Link'], **data)

            elif row["Class"] == 'Algorithm':
                driver_l.insert_instance(row["Title"], class_type=row["Class"],
                                         short_description=row['Brief description'],
                                         keyword=row['Related tags'], license_of_distribution=row['Licence'],
                                         description=row['Main characteristics'],
                                         detailed_description=row['Detailed description'],
                                         gdpr_requirements=row['GDPR requirements'],
                                         trustworthy_AI=row['Trustworthy AI'],
                                         url=row['Link'], **data)
            elif row["Class"] == 'Dataset':
                driver_l.insert_instance(row["Title"], class_type=row["Class"],
                                         short_description=row['Brief description'],
                                         keyword=row['Related tags'], license_of_distribution=row['Licence'],
                                         description=row['Main characteristics'],
                                         detailed_description=row['Detailed description'],
                                         gdpr_requirements=row['GDPR requirements'],
                                         trustworthy_AI=row['Trustworthy AI'],
                                         url=row['Link'], **data)
            elif row["Class"] == 'Library':
                driver_l.insert_instance(row["Title"], class_type=row["Class"],
                                         short_description=row['Brief description'],
                                         keyword=row['Related tags'], license_of_distribution=row['Licence'],
                                         description=row['Main characteristics'],
                                         detailed_description=row['Detailed description'],
                                         gdpr_requirements=row['GDPR requirements'],
                                         trustworthy_AI=row['Trustworthy AI'],
                                         url=row['Link'], **data)
            elif row["Class"] == 'Tool':
                driver_l.insert_instance(row["Title"], class_type=row["Class"],
                                         short_description=row['Brief description'], keyword=row['Related tags'],
                                         license_of_distribution=row['Licence'],
                                         description=row['Main characteristics'],
                                         detailed_description=row['Detailed description'],
                                         gdpr_requirements=row['GDPR requirements'],
                                         trustworthy_AI=row['Trustworthy AI'],
                                         url=row['Link'], **data)
            else:
                raise ValueError("class not recognized")


def insert_ai4eu_organisations(driver_l: Driver, dataset_url):
    # AI4EU assets dataset
    dataset = pd.read_csv(dataset_url, sep=';')
    companies = list(set(dataset['Developed by'].tolist()))
    # insert organisations that are not already in the db
    for company in companies:
        if driver_l.query_node_by_name_class(company, 'Organisation') is None:
            driver_l.insert_instance(company, class_type="Organisation")


if __name__ == "__main__":
    # init the driver
    driver = Driver()
    dataset_lnk = './data/AI4EU_Assets_descriptions.csv'
    categories_lnk = './data/categories_map.csv'

    driver.hard_db_reset()

    # insert all the assets and organisations extracted from the AI4EU old platform (only if they not exist)
    #driver.hard_db_reset(with_backup=False)
    #insert_ai4eu_organisations(driver, dataset_url=dataset_lnk)
    #insert_ai4eu_assets(driver, dataset_url=dataset_lnk, categories_url=categories_lnk)
