"""

Demo examples for performing ontology matching for assets.

"""

import os

from dev_startkit.labeler.data import Asset, Query
from dev_startkit.labeler.data_manager import DataManager
from dev_startkit.project_manager import ProjectManager
from dev_startkit.labeler.unibo.labeler import SBertLabeler
from dev_startkit.labeler.tilde.labeler import ZeroShotLabeler
from dev_startkit.labeler.baselines.labeler import RandomLabeler
from dev_startkit.labeler.classifier import LabelerClassifier
from dev_startkit.utility.evaluation_utils import SklearnMetric

if __name__ == '__main__':

    # Step 0: Configuration
    project_manager_config_file = None

    project_manager = ProjectManager.get_manager()
    project_manager.initialize(config_file=project_manager_config_file)

    # Step 1: Configuring TextLabeler
    # Currently supported: unibo, tilde, random
    labeler_type = 'unibo'
    if labeler_type == 'unibo':
        labeler_info = {
            'labels_filepath': os.path.join(project_manager.local_datasets_dir, 'labels', 'labels.json'),
            'strategy': 'descr2text',
            'similarity_metric': 'cosine_similarity',
            'save_path': None
        }
        score_additional_args = {
            'return_sentences': True,
            'return_amount': 3
        }

        labeler = SBertLabeler(**labeler_info)
    elif labeler_type == 'tilde':
        labeler_info = {
            'labels_filepath': os.path.join(project_manager.local_datasets_dir, 'labels', 'labels.json'),
            'save_path': None
        }
        score_additional_args = {
        }

        labeler = ZeroShotLabeler(**labeler_info)
    elif labeler_type == 'random':
        labeler_info = {
            'labels_filepath': os.path.join(project_manager.local_datasets_dir, 'labels', 'labels.json'),
            'save_path': None
        }
        score_additional_args = {
        }

        labeler = RandomLabeler(**labeler_info)
    else:
        raise RuntimeError(f'Unsupported labeler! Got {labeler_type} but currently supporting [unibo, tilde, random]')

    labeler.configure(force_reload=True)

    # Step 2: Loading data (queries and assets)
    compute_classification_metrics = True
    label_list = [label.name for label in labeler.labels]

    # Assets
    assets_path = os.path.join(project_manager.local_datasets_dir, 'resources', 'ai-catalog-v2.csv')
    assets_manager_info = {
        'columns_mapping': {
            'name': 'Name',
            'description': 'Description',
        },
        'save_path': None,
        'load_labels': compute_classification_metrics,
        'labels': labeler.labels
    }
    assets_data_manager = DataManager(**assets_manager_info)
    assets_data_manager.load_data(data_path=assets_path, data_type=Asset, force_reload=True)

    # Queries
    queries_path = os.path.join(project_manager.local_datasets_dir, 'queries', 'use-cases-v2.csv')
    queries_manager_info = {
        'columns_mapping': {
            'description': 'description',
            'area': 'area',
            'source': 'source'
        },
        'save_path': None,
        'load_labels': compute_classification_metrics,
        'labels': labeler.labels
    }
    query_data_manager = DataManager(**queries_manager_info)
    query_data_manager.load_data(data_path=queries_path, data_type=Query, force_reload=True)

    # Step 3: Perform ontology matching
    # For each data -> a dictionary where
    #   key -> label identifier
    #   value -> matching score
    # We can build Resource objects with this data
    print_amount = 3

    # 3.1 -> Assets ontology matching
    assets_scores = labeler.score(text_list=assets_data_manager.to_key_list(key='description'),
                                  **score_additional_args)
    for asset_score in assets_scores[:print_amount]:
        print(asset_score)
        print()

    # 3.2 -> Query ontology matching
    query_scores = labeler.score(text_list=query_data_manager.to_key_list(key='description'),
                                 **score_additional_args)
    for query_score in query_scores[:print_amount]:
        print(query_score)
        print()

    # [Optional] Compute classification metrics
    metrics = [
        SklearnMetric(name='F1', function_name='f1_score', metric_arguments={'average': 'macro'}),
        SklearnMetric(name='ACC', function_name='accuracy_score')
    ]

    if compute_classification_metrics:
        classifier = LabelerClassifier(metrics=metrics)

        # Assets classification metrics
        assets_labels = assets_data_manager.to_key_list(key='labels')
        classifier.classify(scores=assets_scores, labels=assets_labels)
        classifier.show()

        # Query classification metrics
        query_labels = assets_data_manager.to_key_list(key='labels')
        classifier.classify(scores=query_scores, labels=query_labels)
        classifier.show()
