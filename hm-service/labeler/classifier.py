import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from dev_startkit.utility.log_utils import Logger


class LabelerClassifier(object):

    def __init__(self, metrics, thresholds=None, start_range=0.2, end_range=1.05, step=0.05):
        self.metrics = metrics
        self.thresholds = thresholds if thresholds is not None else np.arange(start_range, end_range, step)

        # Use to store results
        self.metric_values = None
        self.samples = None

    def classify(self, scores, labels, thresholds=None):
        """
        Computes classification metrics for a given set of labeler scores and corresponding ground-truth labels.
        Classification predictions are obtained from labeler scores by applying thresholds.
        """

        thresholds = thresholds if thresholds is not None else self.thresholds

        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Computing classification metrics..\n'
                                         f'Scores: {len(scores)}\n'
                                         f'Thresholds: {self.thresholds}\n'
                                         f'Metrics: {self.metrics}')

        label_ordering = sorted(list(labels[0].keys()))

        metric_values = {}
        for threshold in tqdm(thresholds):
            for score_set, label_set in zip(scores, labels):
                threshold_score_set = {key: 1 if np.max(value['scores']) >= threshold else 0
                                       for key, value in score_set.items()}

                # Sanity check regarding labels
                assert set(label_set.keys()).difference(score_set.keys()) == set()

                y_pred = [threshold_score_set[label] for label in label_ordering]
                y_true = [label_set[label] for label in label_ordering]

                # Skip due to missing annotations
                if np.sum(y_true) == 0:
                    continue

                for metric in self.metrics:
                    metric_value = metric(y_pred=y_pred, y_true=y_true)
                    metric_values.setdefault(metric.name, {}).setdefault(threshold, []).append(metric_value)

        # Compute average metric score for each threshold value
        for metric_name in metric_values:
            for threshold in thresholds:
                if self.samples is None:
                    self.samples = len(metric_values[metric_name][threshold])
                metric_values[metric_name][threshold] = np.mean(metric_values[metric_name][threshold])

        self.metric_values = metric_values

    def show(self):
        assert self.metric_values is not None, f'No metric values were computed. Did you run classify()?'

        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Showing classification metrics...\n'
                                         f'Samples: {self.samples}')

        fig, axs = plt.subplots(1, 1)
        legend = []
        for metric_name, metric_value in self.metric_values.items():
            axs.plot(self.thresholds, [metric_value[threshold] for threshold in self.thresholds])
            legend.append(metric_name)

        axs.set_xlabel('Threshold')
        axs.set_ylabel('Percentage')
        axs.legend(legend)

        plt.show()
