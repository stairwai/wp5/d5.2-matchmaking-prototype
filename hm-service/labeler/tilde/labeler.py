from typing import List, Dict, AnyStr, Any

from tqdm import tqdm
from transformers import pipeline

from dev_startkit.labeler.labeler import TextLabeler
from dev_startkit.utility.log_utils import Logger


class ZeroShotLabeler(TextLabeler):

    def __init__(self, zero_shot_model_name: AnyStr = 'cross-encoder/nli-roberta-base', **kwargs):
        """
        Instantiates a ZeroShotLabeler object.

        Args:
            labels_filepath (str): the path where labels (in textual format) are stored.
            zero_shot_model_name (str): the name of the zero-shot classifier model to use for ontology matching.
            save_path (str): the path where to save or load SBertLabeler data (e.g., configuration files).
        """

        super(ZeroShotLabeler, self).__init__(**kwargs)
        self.zero_shot_model_name = zero_shot_model_name
        self.model = pipeline("zero-shot-classification", model=zero_shot_model_name)

    def score(self, text_list: List[AnyStr]) -> List[Dict[AnyStr, Any]]:
        """
        Computes the unsupervised ontology matching for an input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the selected zero-shot classifier model.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Starting ontology matching:\n'
                                         f'Total documents: {len(text_list)}\n'
                                         f'Total ontology labels: {len(self.labels)}\n'
                                         f'Zero-shot model: {self.zero_shot_model_name}')

        zero_shot_classes = [label.name for label in self.labels]

        scores = []
        for text in tqdm(text_list):
            text_scores = self.model(text, zero_shot_classes, multi_label=True)
            text_scores = {label.identifier: {'scores': text_scores['scores'][text_scores['labels'].index(label.name)]}
                           for label in self.labels}
            scores.append(text_scores)

        return scores
