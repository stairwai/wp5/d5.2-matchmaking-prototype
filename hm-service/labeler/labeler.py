import abc
from typing import List, Dict, AnyStr, Union, Any
# from dev_startkit.utility.log_utils import Logger
# import os
# from dev_startkit.utility.json_utils import load_json
# from dev_startkit.utility.pickle_utils import save_pickle, load_pickle
from utility.log_utils import Logger
import os
from utility.json_utils import load_json
from utility.pickle_utils import save_pickle, load_pickle


class Label(object):
    """
    Stores an ontology label and its textual information
    """

    def __init__(self, identifier: int, name: AnyStr, description: AnyStr,
                 synonyms: Union[List[str], None] = None, is_reliable: bool = True):
        """
        Instantiates a Label object.

        Args:
            identifier (int): an integer that uniquely identifies a ontology label
            name (str): the name of the label
            description (str): a long textual description (a few sentences) of the label.
            synonyms (List[str], Optional): a list of names, each being a synonym of the Label's name.
            is_reliable (bool): whether the label is reliable (i.e. can be used for labeling and matchmaking) or not
        """

        self.identifier = identifier
        self.name = name
        self.description = description
        self.synonyms = synonyms
        self.is_reliable = is_reliable

    def __repr__(self):
        return f'label({self.identifier}, {self.name})'


class TextLabeler(metaclass=abc.ABCMeta):
    """
    A labeler for matching input textual documents to ontology labels.
    """

    def __init__(self,
            # labels : List[Label],
            # relations: Dict[str, List] = None,
            # labels_filepath: AnyStr,
            save_path: AnyStr = None):
        """
        Instantiates a TextLabeler object.
        Wrappers for Label and possible label relations are initialized.

        Args:
            label: a list of Label objects
            save_path (str): the path where to save or load SBertLabeler data (e.g., configuration files).
        """
        # self.labels_filepath = labels_filepath
        # self.original_labels = labels
        # self.original_relations = relations
        self.save_path = save_path if save_path is not None else os.getcwd()

        if not os.path.isdir(self.save_path):
            os.makedirs(self.save_path)

        self.labels = []
        self.relations = []

    # def _label_preprocessing(self) -> List[Label]:
    #     """
    #     Preprocessing function for label objects.
    #     This function is meant to be overloaded in TextLabeler implementations

    #     Returns:
    #         a list of SBertLabel objects, one for each loaded label.
    #     """
    #     Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading labels from JSON file...')

    #     labels_data = load_json(self.labels_filepath)['labels']

    #     label_list = []
    #     for label_item in labels_data:
    #         # Synonyms
    #         synonyms = label_item['related terms']
    #         if type(synonyms) != float:
    #             synonyms = synonyms.replace('...', '')
    #             synonyms = synonyms.split(',')
    #             synonyms = [item.strip() for item in synonyms if len(item.strip())]
    #         else:
    #             synonyms = []

    #         label = Label(identifier=int(label_item['identifier']),
    #                       name=label_item['name'],
    #                       description=label_item['description'],
    #                       synonyms=synonyms,
    #                       is_reliable=label_item['is_reliable'])
    #         label_list.append(label)

    #     return label_list

    # # TODO: upgrade when relations are defined
    # def _load_relations_json(self) -> Dict[str, List]:
    #     """
    #     Loads label relations from a .csv file.

    #     Returns:
    #         a dictionary where:
    #             key: relation type
    #             value: a tuple of labels' identifiers. Each tuple denotes that there is a relation
    #             between the reported labels.
    #     """
    #     Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading label relations from JSON file...')

    #     return {}

    # def load_labels(self, labels: List[Label], relations: Dict[str, List], reliable_only: bool = True):
    #     """
    #     Stores input labels and relations as instance attributes.

    #     Args:
    #         labels (List[Label]): a list of Label objects
    #         relations (Dict[str, List]): a dictionary where:
    #             - keys: type of relation
    #             - values: list of tuples (label1, label2)
    #         reliable_only (bool): if enabled, only reliable labels will be considered
    #     """
    #     Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Attempting to store new information:\n'
    #                                      f'Labels: {labels}\n'
    #                                      f'Relations: {relations}')

    #     # Take only reliable labels
    #     if reliable_only:
    #         Logger.get_logger(__name__).info('Considering reliable labels only...')
    #         Logger.get_logger(__name__).info(f'Before -> #Labels = {len(labels)} -- #Relations = {len(relations)}')
    #         labels = [label for label in labels if label.is_reliable]
    #         relations = {key: [pair for pair in value if pair[0].is_reliable and pair[1].is_reliable]
    #                      for key, value in relations.items()}
    #         Logger.get_logger(__name__).info(f'After -> #Labels = {len(labels)} -- #Relations = {len(relations)}')

    #     # Check for unique labels
    #     identifiers = set([label.identifier for label in labels])
    #     assert len(identifiers) == len(labels)

    #     Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Labels and relations successfully stored!')

    #     self.labels = labels
    #     self.relations = relations

    def configure(self, labels : List[Label] = None):
        """
        Configures a TextLabeler instance.
        The configuration process involves loading labels and their relations, possibly with some preprocessing

        Args:
            labels (list of Label objects): list of labels. In case this is left None, the method will
                try to retrieve a cached copy, otherwise, the list of labels will be processed and then
                cached. Subclasses might extend this method to add functionalities
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Configuring...')
        load_path = os.path.join(self.save_path, f'{self.__class__.__name__.lower()}_data.pickle')

        # if self.labels_filepath.endswith('json'):
        #     labels_function = self._load_labels_json
        #     relations_function = self._load_relations_json
        # else:
        #     raise NotImplementedError(f'Labels filepath is not in .json format - Got {self.labels_filepath}.'
        #                               f'This method only supports .json format at the moment.')

        # Support for forthcoming relations
        relations = []

        # Load if not already done
        if not os.path.isfile(load_path) or labels is not None:
            # Load
            # labels = labels_function()
            # labels = self.raw_labels
            # relations = self.raw_relations

            # Save
            loaded_data = {
                'labels': labels,
                'relations': relations 
            }
            save_pickle(load_path, loaded_data)
        else:
            # Load
            Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading serialized labels'
                                             f' and label relations....')

            loaded_data = load_pickle(load_path)
            labels, relations = loaded_data['labels'], loaded_data['relations']

        # Store labels and relations
        self.labels = labels
        self.relations = relations

        # Set
        # self.load_labels(labels=labels, relations=relations)

    @abc.abstractmethod
    def score(self, text_list: List[AnyStr]) -> List[Dict[AnyStr, Any]]:
        """
        Computes ontology matching for each input text in the given list.
        Ontology matching for an input text involves computing similarity scores for each ontology label.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the similarity obtained for that label.
        """
        pass
