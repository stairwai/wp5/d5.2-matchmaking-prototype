from typing import List, Dict, AnyStr, Any

import numpy as np
from tqdm import tqdm

from dev_startkit.labeler.labeler import TextLabeler
from dev_startkit.utility.log_utils import Logger


class RandomLabeler(TextLabeler):

    def score(self, text_list: List[AnyStr]) -> List[Dict[AnyStr, Any]]:
        """
        Computes the unsupervised ontology matching for an input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the selected zero-shot classifier model.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Starting ontology matching:\n'
                                         f'Total documents: {len(text_list)}\n'
                                         f'Total ontology labels: {len(self.labels)}\n')

        scores = []
        for _ in tqdm(text_list):
            text_scores = {label.identifier: {'scores': np.random.random()} for label in self.labels}
            scores.append(text_scores)

        return scores
