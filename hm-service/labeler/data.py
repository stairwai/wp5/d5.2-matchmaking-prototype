from typing import AnyStr, List, Dict, Any
import abc
import re
from dev_startkit.labeler.labeler import Label


class Data(metaclass=abc.ABCMeta):

    def __init__(self, labels: Dict[AnyStr, str] = None):
        self.labels = labels

    @classmethod
    @abc.abstractmethod
    def from_dict(cls, identifier: int, data_dict: Dict[AnyStr, Any],
                  columns_mapping: Dict[AnyStr, AnyStr],
                  load_labels: bool = False, labels: List[Label] = None) -> 'Data':
        """
        Instantiates a Data object from a given dictionary data structure.
        Args:
            identifier (int): unique identifier of the dictionary data structure.
            data_dict (Dict[str, str]): a dictionary data structure that contains the
                                        information to instantiate a specific Data object.
            columns_mapping (Dict[str, str]): a dictionary where:
                key: the attribute's name of the Data class
                value: the key's name of a serialized data structure under which the Data's attribute data is stored.
                For instance, if the serialized data structure is a pandas.DataFrame, the value is the column name that
                contains the data that corresponds to the Data's attribute used as key in the dictionary.
                E.g.: key = description and value = 'my_description_column' in a pandas.DataFrame object.
            load_labels (bool): if enabled, it also loads the set of manual annotations (w.r.t. ontology) based on the
            given set of label names
            labels (List[Label]): list of labels to consider for loading ontology manual annotations.

        Returns:
            a Data object instance.
        """
        pass


class Asset(Data):
    """
    Stores an AI Asset for ontology matching
    """

    def __init__(self, identifier: int, name: AnyStr, description: AnyStr,
                 characteristics: AnyStr = None, tags: List[AnyStr] = None, **kwargs):
        """
        Instantiates an Asset object.

        Args:
            identifier (int): an integer that uniquely identifies an Asset.
            name (str): name of the Asset. Usually, the Asset's title is used.
            description (str): a brief description of the Asset.
            characteristics (str): a detailed description of the Asset about its main characteristics.
            tags (List[str]): a list of tags that categorize the Asset.
        """
        super(Asset, self).__init__(**kwargs)

        self.identifier = identifier
        self.name = name
        self.description = description
        self.characteristics = characteristics
        self.tags = tags

    @classmethod
    def from_dict(cls, identifier: int, data_dict: Dict[AnyStr, Any],
                  columns_mapping: Dict[AnyStr, AnyStr],
                  load_labels: bool = False, labels: List[Label] = None) -> 'Asset':
        name_key = columns_mapping['name']
        description_key = columns_mapping['description']
        characteristics_key = columns_mapping[
            'characteristics'] if 'characteristics' in columns_mapping else None
        tags_key = columns_mapping['tags'] if 'tags' in columns_mapping else None

        # name
        assert name_key in data_dict, f'Column {name_key} could not be found!'
        name = data_dict[name_key]

        # description
        assert description_key in data_dict, f'Column {description_key} could not be found!'
        description = data_dict[description_key]

        # mask hyperlinks
        description = re.sub(r'https?:\/\/.*[\r\n]*', '[LINK]', description, flags=re.MULTILINE)

        # characteristics
        if characteristics_key is not None and characteristics_key in data_dict:
            characteristics = data_dict[characteristics_key]

            # mask hyperlinks
            characteristics = re.sub(r'https?:\/\/.*[\r\n]*', '[LINK]', characteristics, flags=re.MULTILINE)
        else:
            characteristics = None

        # tags
        if tags_key is not None and tags_key in data_dict:
            tags = data_dict[tags_key].split(',') if type(data_dict[tags_key]) != float else []
            tags = [item.strip() for item in tags if len(item.strip())]
        else:
            tags = None

        if load_labels:
            labels = {label.identifier: int(data_dict[label.name]) for label in labels}
        else:
            labels = None

        # Build Asset object
        asset = Asset(identifier=identifier, name=name, description=description,
                      characteristics=characteristics, tags=tags, labels=labels)
        return asset


class Query(Data):
    """
    Stores a use-case (query) for ontology matching and horizontal-matchmaking
    """

    def __init__(self, identifier: int, description: AnyStr, area: AnyStr, source: AnyStr, **kwargs):
        """
        Instantiates a Query object.

        Args:
            identifier (int): an integer that uniquely identifies a Query.
            description (str): the use-case text to employ as query for horizontal-matchmaking.
            area (str): a macro domain area.
            source (str): the URL from which the Query has been retrieved.
        """
        super(Query, self).__init__(**kwargs)

        self.identifier = identifier
        self.description = description
        self.area = area
        self.source = source

    @classmethod
    def from_dict(cls, identifier: int, data_dict: Dict[AnyStr, Any],
                  columns_mapping: Dict[AnyStr, AnyStr],
                  load_labels: bool = False, labels: List[Label] = None) -> 'Query':
        description_key = columns_mapping['description']
        area_key = columns_mapping['area']
        source_key = columns_mapping['source']

        # description
        assert description_key in data_dict, f'Column {description_key} could not be found!'
        description = data_dict[description_key]

        # mask hyperlinks
        description = re.sub(r'https?:\/\/.*[\r\n]*', '[LINK]', description, flags=re.MULTILINE)

        # area
        assert area_key in data_dict, f'Column {area_key} could not be found!'
        area = data_dict[area_key]

        # source
        assert source_key in data_dict, f'Column {source_key} could not be found!'
        source = data_dict[source_key]

        if load_labels:
            labels = {label.identifier: int(data_dict[label.name]) for label in labels}
        else:
            labels = None

        # Build Query object
        query = Query(identifier=identifier, description=description,
                      source=source, area=area, labels=labels)
        return query
