import os
from typing import AnyStr, List, Dict, Any, Type

import pandas as pd

from dev_startkit.labeler.data import Data
from dev_startkit.utility.pickle_utils import save_pickle, load_pickle
from dev_startkit.utility.log_utils import Logger
from dev_startkit.labeler.labeler import Label


class DataManager(object):
    """
    A wrapper of multiple Data objects.
    """

    def __init__(self, columns_mapping: Dict[AnyStr, AnyStr],
                 save_path: AnyStr = None, load_labels: bool = False, labels: List[Label] = None):
        """
        Instantiates a DataManager object.

        Args:
            columns_mapping (Dict[str, str]): a dictionary where:
                key: the attribute's name of the Data class
                value: the key's name of a serialized data structure under which the Data's attribute data is stored.
                For instance, if the serialized data structure is a pandas.DataFrame, the value is the column name that
                contains the data that corresponds to the Data's attribute used as key in the dictionary.
                E.g.: key = description and value = 'my_description_column' in a pandas.DataFrame object.
            save_path (str): the path where to save the DataManager's instance information. For instance,
            loaded Data objects are serialized to avoid further loading operations.
            load_labels (bool): if enabled, it also loads the set of manual annotations (w.r.t. ontology) based on the
            given set of label names
            labels (List[Label]): list of labels to consider for loading ontology manual annotations.
        """

        self.columns_mapping = columns_mapping
        self.save_path = save_path if save_path is not None else os.getcwd()

        self.data_list = []

        if save_path is not None and os.path.isdir(self.save_path):
            os.makedirs(self.save_path)

        self.load_labels = load_labels
        self.labels = labels

        if self.load_labels:
            assert labels is not None, f"Loading manual annotations requires a list of labels! Got {labels}"

    # TODO: separate text pre-processing from text loading
    def _load_data_from_csv(self, data_path: AnyStr, data_type: Type[Data]) -> List[Data]:
        """
        Reads a .csv file containing information about data and builds a list of Data objects with read information.

        Args:
            data_path (str): the path where the .csv file containing data information is stored.
            data_type (Data): the class type that is an instance of the Data interface.

        Returns:
            a list of Data objects, each one being built from a row of the .csv file.
        """

        data_df = pd.read_csv(data_path)
        data_list = []

        Logger.get_logger(__name__).info(f'Loading {data_df.shape[0]} data items...')
        for row_idx, row in data_df.iterrows():
            row = row.fillna(0)
            data = data_type.from_dict(identifier=row_idx,
                                       columns_mapping=self.columns_mapping,
                                       data_dict=row,
                                       load_labels=self.load_labels,
                                       labels=self.labels)
            data_list.append(data)

        return data_list

    def to_key_list(self, key: AnyStr) -> List[Any]:
        """
        Gets the value of the attribute given as input (key) from each stored Data.

        Args:
            key (str): the name of an attribute of the Data class.

        Returns:
            the value of the given attribute from each stored Data object.
        """

        return [getattr(asset, key) for asset in self.data_list]

    def load_data(self, data_path: AnyStr, data_type: Type[Data], force_reload: bool = False):
        """
        Loads assets information from a serialized data structure.

        Args:
            data_path (str): the path where the serialized data structure containing data information is stored.
            data_type (Data): the class type that is an instance of the Data interface.
            force_reload (bool): forces runtime data loading if True.
        """

        assert os.path.isfile(data_path)

        Logger.get_logger(__name__).info(f'Loading data of type = {data_type.__name__} ...')
        # Loading from .csv
        if data_path.endswith('.csv'):
            load_path = os.path.join(self.save_path, f'{data_type.__name__.lower()}_data_manager.pickle')

            if not os.path.isfile(load_path) or force_reload:
                self.data_list = self._load_data_from_csv(data_path=data_path,
                                                          data_type=data_type)

                # Save
                save_pickle(load_path, self.data_list)
            else:
                # Load
                self.data_list = load_pickle(load_path)
        else:
            raise NotImplementedError(f'Data filepath is not in .csv format - Got {data_path}.'
                                      f'This method only supports .csv format at the moment.')
