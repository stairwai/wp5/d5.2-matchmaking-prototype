from typing import List, Dict, AnyStr, Any

import numpy as np
import spacy
from sentence_transformers import SentenceTransformer
from tqdm import tqdm

# from dev_startkit.labeler.labeler import TextLabeler, Label
# from dev_startkit.utility.json_utils import load_json
# from dev_startkit.utility.log_utils import Logger
# from dev_startkit.utility.metric_utils import get_metric
from labeler.labeler import TextLabeler, Label
from utility.json_utils import load_json
from utility.log_utils import Logger
from utility.metric_utils import get_metric
from subprocess import Popen


class SBertLabel(Label):
    """
    An extension of the standard Label to support S-BERT embeddings
    """

    def __init__(self, description_sentences: List[str], **kwargs):
        """
        Instantiates a SBertLabel object.

        Args:
            description_sentences (List[str]): the text description of a Label split into sentences.
        """

        super(SBertLabel, self).__init__(**kwargs)
        self.description_sentences = description_sentences

        # Embedding
        self.name_embedding = None
        self.description_sentences_embedding = None
        self.synonyms_embedding = None

    def embed(self, sbert_model: SentenceTransformer):
        """
        Encodes a Label's textual data with a S-BERT model.
        Embeddings are stored as instance attributes to avoid further re-computations.

        Args:
            sbert_model (SentenceTransformer): an instance of a S-BERT model.
        """

        self.name_embedding = sbert_model.encode([self.name])
        self.description_sentences_embedding = sbert_model.encode(self.description_sentences)
        if len(self.synonyms):
            self.synonyms_embedding = sbert_model.encode(self.synonyms)


class SBertLabeler(TextLabeler):
    """
    An extension of the standard TextLabeler to support S-BERT encoding.
    """

    def __init__(self, spacy_model_name: AnyStr = 'en_core_web_sm',
                 strategy: AnyStr = 'descr2text', sbert_model_name: AnyStr = 'all-mpnet-base-v2',
                 similarity_metric: AnyStr = 'cosine_similarity', **kwargs):
        """
        Instantiates a SBertLabeler object.

        Args:
            spacy_model_name (str): the name of the SpaCy tokenizer model to use for sentence splitting.
            strategy (str): the name of strategy to use for performing unsupervised ontology matching.
            sbert_model_name (str): the name of a S-BERT model to instantiate for embedding textual data.
            similarity_metric (str): the name of the similarity metric to adopt for comparing embedding vectors.
        """

        super(SBertLabeler, self).__init__(**kwargs)
        self.spacy_model_name = spacy_model_name
        self.spacy_model = self._setup_spacy_model(spacy_model_name=spacy_model_name)
        self.spacy_model.add_pipe('sentencizer')
        self.strategy = strategy
        self.sbert_model_name = sbert_model_name
        self.sbert_model = SentenceTransformer(sbert_model_name)
        self.similarity_metric, self.is_metric_embedding_based = get_metric(similarity_metric)

        self.score_strategy_map = {
            'name2text': self._score_name2text,
            'descr2text': self._score_descr2text,
        }

        assert strategy in self.score_strategy_map.keys(), \
            f"Strategy not supported! Got {strategy}, but currently supporting {list(self.score_strategy_map.keys())}"

    def _setup_spacy_model(self, spacy_model_name):
        if not spacy.util.is_package(spacy_model_name):
            cmd = ['python',
                   '-m spacy',
                   f'download {spacy_model_name}']

            cmd = ' '.join(cmd)
            Logger.get_logger(__name__).info(f'Downloading spacy model...\n{cmd}')

            # Run process
            process = Popen(cmd, shell=True)
            process.wait()

        return spacy.load(spacy_model_name)

    def _sentence_split_and_filter(self, text: AnyStr) -> List[str]:
        """
        Splits a text into sentences and filters out empty sentences.

        Args:
            text (str): the text to be split into sentences.

        Returns:
             a list of sentences extracted by using the SpaCy tokenizer.
        """

        sentences = [sentence.text for sentence in self.spacy_model(text).sents if len(sentence.text.strip())]
        return sentences

    # def _load_labels_json(self) -> List[Label]:
    def configure(self, labels : List[Label] = None):
        # """
        # Loads label textual data from a .json file.
        # For each loaded label, a SBertLabel instance is created and stored in a list.

        # Returns:
        #     a list of SBertLabel objects, one for each loaded label.
        # """
        """
        Configures a TextLabeler instance.
        The configuration process involves loading labels and their relations with some preprocessing.

        Args:
            labels (list of Label objects): list of labels. In case this is left None, the method will
                try to retrieve a cached copy, otherwise, the list of labels will be processed and then
                cached. Subclasses might extend this method to add functionalities
        """

        if labels is None:
            # Trigger parent configuration
            super(SBertLabeler, self).configure(labels)
        else:
            Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Preprocessing labels...')
            sbert_labels = []
            for label in labels:
                # Sentence split
                description_sentences = self._sentence_split_and_filter(text=label.description)
                # Synonyms
                synonyms = label.synonyms if label.synonyms is not None else []
                # Build the specialized label
                sbert_label = SBertLabel(identifier=label.identifier,
                                   name=label.name,
                                   description=label.description,
                                   description_sentences=description_sentences,
                                   synonyms=synonyms,
                                   is_reliable=True)
                sbert_label.embed(self.sbert_model)
                # Store
                sbert_labels.append(sbert_label)
            
            # Trigger parent configuration
            super(SBertLabeler, self).configure(sbert_labels)


    def _score_name2text(self, text_sentences: List[AnyStr], return_sentences: bool = False,
                         return_amount: int = 1) -> Dict[AnyStr, Dict[AnyStr, Any]]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'name2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's name and synonyms.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).
            return_sentences (bool): if enabled, the sentence corresponding to the best label-to-input score will
            be returned along with its similarity score.
            return_amount: amount of values for each label to return

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # Sanity checks
        assert self.labels is not None, "No label data found. " \
                                        "Perhaps, you have to load label data by running the configure() method."

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        for label in self.labels:
            if not hasattr(label, 'name'):
                raise RuntimeError(f"Could not find attribute 'name' for label. Label object attributes: {vars(label)}")

            if self.is_metric_embedding_based:
                label_embedding = label.name_embedding
            else:
                label_embedding = label.name

            # Consider synonyms (if available)
            if len(label.synonyms):
                if self.is_metric_embedding_based:
                    label_embedding = np.vstack((label_embedding, label.synonyms_embedding))
                else:
                    label_embedding = [label_embedding] + label.synonyms

            # Similarity
            label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding).ravel()

            return_amount = min(len(label_similarities), return_amount)

            # Take top (return_amount) scores
            top_indexes = np.argpartition(label_similarities, -return_amount)[-return_amount:]
            top_sorted_indexes = top_indexes[np.argsort(label_similarities[top_indexes])][::-1]
            top_values = label_similarities[top_sorted_indexes]

            column_indexes = np.apply_along_axis(lambda x: x % len(text_sentences), axis=0, arr=top_sorted_indexes)
            top_sentences = text_sentences[column_indexes]

            # Memorize
            scores[label.identifier] = {
                'scores': top_values
            }

            # Add sentences corresponding to top scores as well
            if return_sentences:
                scores[label.identifier]['sentences'] = top_sentences
                scores[label.identifier]['sentences_amount'] = len(text_sentences)

        return scores

    def _score_descr2text(self, text_sentences: List[AnyStr], return_sentences: bool = False,
                          return_amount: int = 1) -> Dict[AnyStr, Dict[AnyStr, Any]]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'descr2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's textual description.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).
            return_sentences (bool): if enabled, the sentence corresponding to the best label-to-input score will
            be returned along with its similarity score.
            return_amount: amount of values for each label to return

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        for label in self.labels:
            # Skip malformed labels
            if not label.description_sentences:
                continue

            if self.is_metric_embedding_based:
                label_embedding = label.description_sentences_embedding
            else:
                label_embedding = label.description_sentences

            # Similarity
            label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding).ravel()

            return_amount = min(len(label_similarities), return_amount)

            # Take top (return_amount) scores
            top_indexes = np.argpartition(label_similarities, -return_amount)[-return_amount:]
            top_sorted_indexes = top_indexes[np.argsort(label_similarities[top_indexes])][::-1]
            top_values = label_similarities[top_sorted_indexes]

            column_indexes = np.apply_along_axis(lambda x: x % len(text_sentences), axis=0, arr=top_sorted_indexes)
            top_sentences = np.array(text_sentences)[column_indexes]

            row_indexes = np.apply_along_axis(lambda x: x // len(text_sentences), axis=0, arr=top_sorted_indexes).astype(np.int32)
            top_label_sentences = np.array(label.description_sentences)[row_indexes]

            assert len(top_sentences) == len(top_label_sentences), \
                f'Inconsistent number of sentences for pair definition.' \
                f'Query sentences {len(top_sentences)} -- label sentences {len(top_label_sentences)}'

            # Memorize
            scores[label.identifier] = {
                'scores': top_values
            }

            # Add sentences corresponding to top scores as well
            if return_sentences:
                scores[label.identifier]['sentence_pairs'] = list(zip(top_sentences.tolist(), top_label_sentences.tolist()))
                scores[label.identifier]['sentences_amount'] = len(text_sentences)

        return scores

    def score(self, text_list: List[Any], return_sentences: bool = False,
              return_amount: int = 1) -> List[Dict[AnyStr, Dict[AnyStr, Any]]]:
        """
        Computes the unsupervised ontology matching for a input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the SBertLabeler's strategy.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).
            return_sentences (bool): if enabled, the sentence corresponding to the best label-to-input score will
            be returned along with its similarity score.
            return_amount: amount of values for each label to return

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Starting ontology matching:\n'
                                         f'Total documents: {len(text_list)}\n'
                                         f'Total ontology labels: {len(self.labels)}\n'
                                         f'Strategy: {self.strategy}\n'
                                         f'S-BERT model: {self.sbert_model}')

        # Make sure return_amount is always greater than or equal to 1
        return_amount = max(return_amount, 1)

        scores = []
        for text in tqdm(text_list):
            # Get sentences
            text_sentences = self._sentence_split_and_filter(text=text)

            text_scores = self.score_strategy_map[self.strategy](text_sentences=text_sentences,
                                                                 return_sentences=return_sentences,
                                                                 return_amount=return_amount)
            scores.append(text_scores)

        return scores
