import os
import importlib.util
import sys
import json
from utility import hm_utils
from project_manager import ProjectManager
from ams_api.kr.StairwAIDriver import Driver

if __name__ == "__main__":
    # Init the driver
    print('>>> Initializing the driver')
    driver = Driver()
    print('<<< Driver initilized')

    # Use this during development, when a DB reset is needed
    #driver.hard_db_reset()

    # Read configuration parameters
    print('>>> Reading configuration parameters')
    params = None
    with open('conf.json') as fp:
        params = json.load(fp)
    print('<<< Configuration parameters read')

    # Setup the project manager
    manager = ProjectManager.get_manager()
    manager.initialize()

    print('>>> Loading Labels')
    labels = hm_utils.load_labels_ams(driver)
    label_dict = {l.identifier : l for l in labels}
    print('<<< Labels loaded')

    # Load resources
    print('>>> Loading resources (from a static file)')
    resource_fname = os.path.normpath(params['resource_fname'])
    resources = hm_utils.load_resources(resource_fname)
    if 'indexed_resource_limit' in params and params['indexed_resource_limit'] > 0:
        limit = min(len(resources), params['indexed_resource_limit'])
        resources = resources[:limit]
    print('<<< Loading resources')

    # Load the labeler
    print('>>> Loading the labeler')
    labeler = hm_utils.build_unibo_labeler(labels)
    print('>>> Labeler loaded')

    print('>>> Labeling resources...')
    # Label the resources
    resource_descriptions = [d for d in resources['Description']]

    # Score post-processing
    # resource_scores_raw = labeler.score(resource_descriptions, return_sentences=True, return_amount=3)
    # resource_scores = [{k: v['scores'].min() for k, v in V.items()} for V in resource_scores_raw]
    resource_scores = hm_utils.score_queries(resource_descriptions, labeler)
    resource_list = hm_utils.build_resource_index(resources, resource_scores)

    print('<<< Labeling resources...')

    print('>>> Storing scores on the server...')

    # Update AI expert scores
    for resource in resource_list:
        # Check if the node already exist (Which function?)
        # If it does not exist: create the node and store the scores
        # if it exists: update the scores
        name = resource.name
        node = driver.query_node_by_name_class(name=name, ont_class="AI expert")
        if node is not None:
            node_id = node['swai__identifier']
            print('Resource {} already exists.'.format(name))
        else:
            print('Inserting resource {}.'.format(name))
            expert_params = {
                        'swai__name': resource.name,
                        'swai__short_description': resource.description,
                        'swai__scored_in': {label_dict[l].name: v for l, v in resource.scores.items()}
                    }
            # Insert expert node with description and scores
            node = driver.insert_instance(class_type='AI expert', param_dict=expert_params)
            # Store node swai_identifier
            node_id = node['swai__identifier']
            resource.identifier = node_id
            print("Created node {} with swai id {}".format(name,node_id))

    print('>>> Scores stored...')

print('THE END')



