from py2neo import Graph
import py2neo
import os
import sys
import time

def get_env_var(name, default=None):
    if name in os.environ:
        return os.environ[name]
    else:
        return default


# get the environment variables
bolt_url = get_env_var('NEO4J_URL', 'bolt://localhost:7687')
# bolt_url = get_env_var('NEO4J_URL', 'bolt://10.10.1.4:7687')
user = get_env_var('NEO4J_USER', 'neo4j')
password = get_env_var('NEO4J_PASSWORD', 's3cr3t')

# init the database
print("Attempting connection to the Neo4j instance...")
sys.stdout.flush()

connected = False
cnt = 1

while not connected:
    try:
        print(f"(Trial {cnt})")
        sys.stdout.flush()
        driver = Graph(bolt_url, auth=(user, password))
        connected = True
    except py2neo.errors.ConnectionUnavailable:
        time.sleep(3)

# Check whether the DB is already initilized
if len(driver.nodes.match("owl__Class").all()) > 0:
    print('The DB is already initilized')
    sys.stdout.flush()
else:
    # import the ontologies required and create the prefixes
    print("Initialising the Neo4j database...")
    sys.stdout.flush()

    driver.run("CALL n10s.graphconfig.init();")
    driver.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")

    print("Initialising prefixes...")
    sys.stdout.flush()

    driver.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
    driver.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
    driver.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
    driver.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")

    print("Loading ontology data...")
    sys.stdout.flush()

    driver.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.ttl', 'RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.ttl', 'RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl', 'RDF/XML');")
