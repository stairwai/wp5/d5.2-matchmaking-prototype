## 1/09/2022
### Author (s):

Federico Ruggeri

### Changelogs

- Unibo labeler now provides top-K ontology matching scores along with corresponding input text sentences (i.e., the query)
- Fixed minor functions signature inconsistencies
- Updated classifier according to new labelers score APIs (to be consistent with Unibo labeler changes)
- Updated demo script
- Merged with main branch

## 16/05/2022
### Author (s):

Federico Ruggeri

### Changelogs

- Added Tilde's text labeler (labeler/tilde/labeler.py)
- Added Random text labeler (labeler/baselines/labeler.py)
- Improved TextLabeler interface (documentation cleaning, signatures update and minor fixes)
- Updated Unibo's text labeler (labeler/unibo/labeler.py)
- Refactored data (assets & queries) loading (labeler/data.py and labeler/data_manager.py)
- Modified labels JSON file with an additional attribute 'is_reliable' to allow label filtering (labelers code is updated accordingly)
- Improved demo script (labeler/demo.py)
- Moved utility folder to root path (dev_startkit/utility) so that it can be used in other subpackages.
- Removed const_define.py and introduced a general-purpose and customizable (via a JSON file) ProjectManager (dev_startkit/project_manager.py) 