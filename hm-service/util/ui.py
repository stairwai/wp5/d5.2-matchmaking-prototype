from IPython.display import display, HTML
import ipywidgets as widgets
import numpy as np
import copy

def display_online_matchmaking_results(query, resource_index, mm_res, max_resources=None):
    # Display query description
    display(HTML(f'<strong>Query:</strong> {query}'))
    # Cap the number of displayed resources
    if max_resources is not None and max_resources < len(mm_res):
        mm_res = mm_res[:max_resources]
    # Convert the resource index to a dictionary
    res_dict = {r.identifier: r.name for r in resource_index}
    # Display resources
    display(HTML(f'<strong>Resources:</strong>'))
    display(HTML(f'<ul>'))
    for idx, score in mm_res:
        display(HTML(f'<li>{res_dict[idx]} <i>(score: {score:.3f})</i></li>'))
    display(HTML(f'</ul>'))


class LabelingUI:
    def __init__(self, identifiers, descriptions, scores, labels, n_prelabel=5):
        # Configuration fields
        self.identifiers = identifiers
        self.descriptions = descriptions
        self.scores = scores
        self.labels = labels
        self.n_prelabel = n_prelabel

        # Main results
        self.results = {}

        # UI element to show the description
        self.ui_label = widgets.HTML(value=f'<strong>Description:</strong> ...')
        # UI elements for labeling
        self.ui_rcws = []
        for lbl in labels:
            rcw = widgets.RadioButtons(
                options=['relevant', '---', 'not relevant'],
                value='---',
                layout={'width': 'max-content'}, # If the items' names are long
                description=f'{lbl.name}',
                disabled=False
            )
            self.ui_rcws.append(rcw)

        # Forward button
        self.ui_fwd = widgets.Button(
            description='Next',
            disabled=False,
            button_style='', # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Next item',
            icon='forward-step' # (FontAwesome names without the `fa-` prefix)
        )

        # Register forward click callback
        def on_click_fwd(b):
            self._store_results()
            self._set_index(min(self.index+1, len(self.identifiers)-1))

        self.ui_fwd.on_click(on_click_fwd)

        # Backward button
        self.ui_bwd = widgets.Button(
            description='Previous',
            disabled=False,
            button_style='', # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Previous item',
            icon='backward-step' # (FontAwesome names without the `fa-` prefix)
        )
        self.ui_control_box = widgets.HBox([self.ui_bwd, self.ui_fwd])

        # Register backward click callback
        def on_click_bwd(b):
            self._store_results()
            self._set_index(max(self.index-1, 0))

        self.ui_bwd.on_click(on_click_bwd)

        # UI container (grid)
        layout = widgets.Layout(grid_template_columns="repeat(4, 230px)")
        self.ui_grid = widgets.GridBox(self.ui_rcws, layout=layout)

        # UI container (main box)
        self.ui_main = widgets.VBox([self.ui_label, self.ui_grid, self.ui_control_box])

        # Internal status
        self._set_index(0)


    def _store_results(self):
        res = {}
        for k, lbl in enumerate(self.labels):
            if self.ui_rcws[k].value == 'relevant':
                res[lbl.identifier] = 1
            elif self.ui_rcws[k].value == 'not relevant':
                res[lbl.identifier] = -1
            else:
                res[lbl.identifier] = 0
        self.results[self.index] = res


    def _set_index(self, val):
        assert(0 <= val < len(self.identifiers))
        # Set index
        self.index = val

        # Update description
        desc = self.descriptions[val]
        self.ui_label.value = f'<strong>Description:</strong> {desc}'

        # Update labels
        if val in self.results:
            res = self.results[val]
            for k, lbl in enumerate(self.labels):
                if res[lbl.identifier] == 1:
                    self.ui_rcws[k].value = 'relevant'
                elif res[lbl.identifier] == -1:
                    self.ui_rcws[k].value = 'not relevant'
                else:
                    self.ui_rcws[k].value = '---'
        else:
            # Sort labels by increasing scores
            lidx = sorted(self.scores[val].keys(), key=lambda k: self.scores[val][k])
            highest = lidx[-self.n_prelabel:]
            lowest = lidx[:self.n_prelabel]
            for k, lbl in enumerate(self.labels):
                if lbl.identifier in highest:
                    self.ui_rcws[k].value = 'relevant'
                elif lbl.identifier in lowest:
                    self.ui_rcws[k].value = 'not relevant'
                else:
                    self.ui_rcws[k].value = '---'

        # Control forward button state
        if val == len(self.identifiers)-1:
            self.ui_fwd.disabled = True
        else:
            self.ui_fwd.disabled = False
        # Control backward button state
        if val == 0:
            self.ui_bwd.disabled = True
        else:
            self.ui_bwd.disabled = False



    def _ipython_display_(self):
        return self.ui_main._ipython_display_()




class MatchingRefinementUI:
    def __init__(self,
            query_names, query_descriptions,
            resource_names, resource_descriptions,
            all_rankings,
            max_displayed,
            start_matching=None):
        # Configuration fields
        self.all_rankings = all_rankings
        self.query_names = query_names
        self.query_descriptions = query_descriptions
        self.resource_names = resource_names
        self.resource_descriptions = resource_descriptions
        self.max_displayed = max_displayed

        # Main results
        if start_matching is not None:
            self.matching = copy.deepcopy(start_matching)
        else:
            self.matching = {}

        # UI element to show the query description
        self.ui_label = widgets.HTML(value=f'<strong>Query Name:</strong> ...')

        # UI elements for labeling
        self.ui_check = []
        self.ui_check_desc = []
        self.ui_res_boxes = []
        for lbl in range(self.max_displayed):
            # Build the checkbox
            check = widgets.Checkbox(
                value=False,
                description='...',
                disabled=True,
                indent=False,
                # layout={'width': 'max-content'}, # If the items' names are long
                # layout={'width': '100%'}, # If the items' names are long
            )
            self.ui_check.append(check)
            # Build the label
            check_lbl = widgets.HTML(value=f'...')
            self.ui_check_desc.append(check_lbl)
            # Package in a vertical box
            self.ui_res_boxes.append(widgets.VBox([check, check_lbl]))

        # Forward button
        self.ui_fwd = widgets.Button(
            description='Next',
            disabled=False,
            button_style='', # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Next item',
            icon='forward-step' # (FontAwesome names without the `fa-` prefix)
        )

        # Register forward click callback
        def on_click_fwd(b):
            self._store_results()
            self._set_index(min(self.index+1, len(self.query_descriptions)-1))

        self.ui_fwd.on_click(on_click_fwd)

        # Backward button
        self.ui_bwd = widgets.Button(
            description='Previous',
            disabled=False,
            button_style='', # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Previous item',
            icon='backward-step' # (FontAwesome names without the `fa-` prefix)
        )
        self.ui_control_box = widgets.HBox([self.ui_bwd, self.ui_fwd])

        # Register backward click callback
        def on_click_bwd(b):
            self._store_results()
            self._set_index(max(self.index-1, 0))

        self.ui_bwd.on_click(on_click_bwd)

        # UI container (formally a grid, but in practice a single column)
        self.ui_grid = widgets.VBox(self.ui_res_boxes)

        # UI container (main box)
        self.ui_main = widgets.VBox([self.ui_label, self.ui_grid, self.ui_control_box])

        # Internal status
        self._set_index(0)


    def _store_results(self):
        res = set()
        for k, check in enumerate(self.ui_check):
            # If there are enough entries in the ranking
            if k < len(self.all_rankings[self.index]):
                # If the checkbox is selected
                if check.value == True:
                    # Retrieve the resource index
                    res_idx, _ = self.all_rankings[self.index][k]
                    res.add(res_idx)
        self.matching[self.index] = res


    def _set_index(self, val):
        assert(0 <= val < len(self.query_descriptions))
        # Set index
        self.index = val

        # Update description
        q_name = self.query_names[val]
        q_desc = self.query_descriptions[val]
        self.ui_label.value = f'<p><strong>{q_name}</strong> {q_desc}</p>'

        # Update all checkboxes and their descriptions
        for k, check in enumerate(self.ui_check):
            # If there are enough entries in the ranking
            if k < len(self.all_rankings[val]):
                # Enable the control
                check.disabled = False
                # Retrieve the resource data
                res_idx, res_dist = self.all_rankings[val][k]
                res_name = self.resource_names[res_idx]
                res_desc = self.resource_descriptions[res_idx]
                # Update the control description
                dstr = f'<strong>{res_name}({res_dist:.4f})</strong>'
                check.description = dstr
                # Update the label
                self.ui_check_desc[k].value = f'<p style="font-size:8pt"><i>{res_desc}</i></p>'
                # Update the checkbox value
                if val in self.matching and res_idx in self.matching[val]:
                    check.value = True
                else:
                    check.value = False
            else:
                # Disable the control
                check.disabled = True
                check.description = '...'
                self.check_lbl[k].value = '...'

        # Control forward button state
        if val == len(self.query_descriptions)-1:
            self.ui_fwd.disabled = True
        else:
            self.ui_fwd.disabled = False
        # Control backward button state
        if val == 0:
            self.ui_bwd.disabled = True
        else:
            self.ui_bwd.disabled = False


    def _ipython_display_(self):
        return self.ui_main._ipython_display_()
