#!/bin/python3
from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify
from hm import HorizontalMatchmaking
from util import ssac

# ==============================================================================
# Service setup
# ==============================================================================

# Build the Flask app
app = Flask(__name__)
# Initialize HM
print('>>> Initializing the Horizonal Matchmaking services')
hm = HorizontalMatchmaking()
print('<<< Done')

# ==============================================================================
# Routes (HM)
# ==============================================================================

# Define the main route
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/matchmaking', methods=['POST'])
def matchmaking():
    return hm.run_matchmaking(request.form['query'])


# Define a route to display results
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/matchmaking_gui', methods=['GET', 'POST'])
def matchmaking_gui():
    resource_list = None
    query=None
    if request.method == 'POST':
        query = request.form['query']
        resource_list = hm.run_matchmaking(query)
        resource_list = resource_list['assets']
    return render_template('hm_matchmaking_gui.html',
            resource_list=resource_list, query=query)


# A route to run the labeling service
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/labeling', methods=['POST'])
def labeling():
    return hm.run_labeling(request.form['query'])


# Define a route to display labels
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/labeling_gui', methods=['GET', 'POST'])
def labeling_gui():
    label_list = None
    query=None
    if request.method == 'POST':
        query = request.form['query']
        label_list = hm.run_labeling(query)
        label_list = label_list['labels']
    return render_template('hm_labeling_gui.html',
            label_list=label_list, query=query)


# Define a route to collect expert profiles
# @app.route('/hm_asset_expert_gui', methods=['GET', 'POST'])
# def hm_asset_expert_gui():
#     # Configuration parameters
#     ep_dir = hm.params['asset_expert']
#     country_options = hm.get_country_options()
#     # country_options = hm.params['country_options']
#     language_options = hm.params['language_options']
#     # Access control parameters
#     access_granted = False
#     token = None
#     # Default values for the content fields
#     description = None
#     country = None
#     languages = None
#     team_size = None
#     label_list = None
#     # Attempt to retrieve the access token
#     token = request.args.get('token', default=None, type=str)
#     # Perform access control
#     content = ssac.check_and_read(ep_dir, token)
#     if content is not None:
#         access_granted = True
#     # Populate content fields with stored values
#     if content is not None and 'description' in content:
#         description = content['description']
#     if content is not None and 'country' in content:
#         country = content['country']
#     if content is not None and 'languages' in content:
#         languages = content['languages']
#     if content is not None and 'team_size' in content:
#         team_size = content['team_size']
#     # Retrieve freshly entered data
#     if access_granted and request.method == 'POST':
#         # Replace fields with new values
#         if 'description' in request.form:
#             description = request.form['description']
#         else:
#             description = ''
#         if 'country' in request.form:
#             country = request.form['country']
#         else:
#             country = ''
#         if 'languages' in request.form:
#             languages = request.form.getlist('languages')
#         else:
#             languages = []
#         if 'team_size' in request.form:
#             team_size = request.form['team_size']
#         else:
#             team_size = ''
#     # Retrieve ranked labels
#     if access_granted and description is not None:
#         label_list = hm.run_labeling(description)
#         label_list = label_list['labels']
#     # Store new information
#     if access_granted:
#         new_info = {}
#         new_info['description'] = description
#         new_info['country'] = country
#         new_info['languages'] = languages
#         new_info['team_size'] = team_size
#         new_info['label_list'] = label_list
#         ssac.check_and_store(new_info, ep_dir, token)
#     return render_template('hm_asset_expert_gui.html',
#             access_granted=access_granted,
#             action='/hm_asset_expert_gui'+f'?token={token}' if token is not None else '',
#             country_options=country_options,
#             language_options=language_options,
#             description=description,
#             country=country,
#             languages=languages,
#             team_size=team_size,
#             max_length=500,
#             label_list=label_list)

@app.route('/hm_asset_expert_gui', methods=['GET', 'POST'])
def hm_asset_expert_gui():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.asset_expert_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return render_template('hm_access_denied.html')
    # Retrieve the expert ID
    expert_id = content['swai__uuid_identifier']

    # Handle requests =======================================================
    # Add a new expert of update the AMS entry
    if request.method == 'POST':
        res = hm.asset_expert_post(expert_id, request)
    else:
        res = hm.asset_expert_get(expert_id, request)

    # Display the GUI =======================================================
    # Get available country options from the AMS
    country_options = sorted(hm.get_country_options())
    # Get available language options from the AMS
    language_options = [[t['desc'], t['label']] for t in hm.get_language_options()]
    language_options = sorted(language_options, key=lambda t: t[1])
    # Determine the action link
    action='/hm_asset_expert_gui'+f'?token={token}' if token is not None else ''
    # Return the rendered template
    return render_template('hm_asset_expert_gui.html',
            action=action,
            country_options=country_options,
            language_options=language_options,
            expert_id=expert_id,
            description=res['description'],
            country=res['country'],
            languages=res['languages'],
            team_size=res['team_size'],
            max_length=500,
            label_list=res['label_list'])


@app.route('/hm_asset_expert', methods=['GET', 'POST'])
def hm_asset_expert():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.asset_expert_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return jsonify({'msg', 'Invalid of missing access token'}), 403

    # Handle request ========================================================
    if request.method == 'POST':
        # Add a new expert of update the AMS entry
        res = hm.asset_expert_post(request)
    else:
        # Retrieve data about the expert
        res = hm.asset_expert_get(request)
    # Return results
    return jsonify(res), 200



# use case to expert matchmaking (GUI version)
@app.route('/hm_query_usecase_gui', methods=['GET', 'POST'])
def hm_query_usecase_gui():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.query_usecase_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return render_template('hm_access_denied.html')
    # Retrieve the usecase ID
    usecase_id = token

    # Handle requests =======================================================
    # Retrieve or store use case data
    if request.method == 'POST':
        res = hm.query_usecase_post(usecase_id, request)
    else:
        res = hm.query_usecase_get(usecase_id, request)

    # Display the GUI =======================================================
    # Get available country options from the AMS
    country_options = sorted(hm.get_country_options())
    # Get available language options from the AMS
    language_options = [[t['desc'], t['label']] for t in hm.get_language_options()]
    language_options = sorted(language_options, key=lambda t: t[1])
    # Determine the action link
    action='/hm_query_usecase_gui'+f'?token={token}' if token is not None else ''
    return render_template('hm_query_usecase_gui.html',
            action=action,
            language_options=language_options,
            label_options=hm.label_options,
            description=res['description'],
            team_size_min=res['team_size_min'],
            ai_readiness=res['ai_readiness'],
            languages=res['languages'],
            emph_labels=res['emph_labels'],
            max_length=500,
            asset_list=res['asset_list'],
            explanation_list=res['explanation_list'])




# def hm_query_usecase_gui():
#     # Configuration parameters
#     uc_dir = hm.params['matchmaking_usecase']
#     language_options = hm.params['language_options']
#     # Access control parameters
#     access_granted = False
#     token = None
#     # Default values for the content fields
#     description = None
#     languages = None
#     emph_labels = None
#     team_size_min = None
#     ai_readiness = None
#     asset_list = None
#     # Attempt to retrieve the access token
#     token = request.args.get('token', default=None, type=str)
#     # Perform access control
#     content = ssac.check_and_read(uc_dir, token)
#     if content is not None:
#         access_granted = True
#     # Populate content fields with stored values
#     if content is not None and 'description' in content:
#         description = content['description']
#     if content is not None and 'team_size_min' in content:
#         team_size_min = content['team_size_min']
#     if content is not None and 'ai_readiness' in content:
#         ai_readiness = content['ai_readiness']
#     if content is not None and 'languages' in content:
#         languages = content['languages']
#     if content is not None and 'emph_labels' in content:
#         emph_labels = content['emph_labels']
#     # Retrieve freshly entered data
#     if access_granted and request.method == 'POST':
#         # Replace fields with new values
#         if 'description' in request.form:
#             description = request.form['description']
#         else:
#             description = ''
#         if 'team_size_min' in request.form:
#             team_size_min = request.form['team_size_min']
#         else:
#             team_size_min = 0
#         if 'ai_readiness' in request.form:
#             ai_readiness = request.form['ai_readiness']
#         else:
#             ai_readiness = 0
#         if 'languages' in request.form:
#             languages = request.form.getlist('languages')
#         else:
#             languages = []
#         if 'emph_labels' in request.form:
#             emph_labels = [int(l) for l in request.form.getlist('emph_labels')]
#         else:
#             emph_labels = []
#     # Retrieve ranked labels
#     if access_granted and description is not None:
#         metadata = {
#                     'team_size_min' : team_size_min,
#                     'ai_readiness' : ai_readiness,
#                     'languages' : languages,
#                     'emph_labels' : emph_labels
#                 }
#         mm_result = hm.run_matchmaking(description, metadata=metadata,
#                 return_explanations=True)
#         asset_list = mm_result['assets']
#         explanation_list = mm_result['explanations']
#     # Store new information
#     if access_granted:
#         new_info = {}
#         new_info['description'] = description
#         new_info['team_size_min'] = team_size_min
#         new_info['ai_readiness'] = ai_readiness
#         new_info['languages'] = languages
#         new_info['emph_labels'] = emph_labels
#         new_info['asset_list'] = asset_list
#         ssac.check_and_store(new_info, uc_dir, token)
#     return render_template('hm_query_usecase_gui.html',
#             access_granted=access_granted,
#             action='/hm_query_usecase_gui'+f'?token={token}' if token is not None else '',
#             language_options=language_options,
#             label_options=hm.label_options,
#             description=description,
#             team_size_min=team_size_min,
#             ai_readiness=ai_readiness,
#             languages=languages,
#             emph_labels=emph_labels,
#             max_length=500,
#             asset_list=asset_list,
#             explanation_list=explanation_list)


if __name__ == "__main__":
    # app.run(ssl_context=('cert.pem', 'key.pem'))
    app.run()
