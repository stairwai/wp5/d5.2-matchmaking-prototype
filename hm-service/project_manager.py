import os


class ProjectManager(object):

    DEFAULT_PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
    DEFAULT_LOCAL_DATASETS_DIR = 'data'
    DEFAULT_LOGGING_DIR = 'log'
    DEFAULT_LOGGING_FILENAME = 'daily_log.log'

    _instance = None

    def __init__(self):
        self.project_dir = ProjectManager.DEFAULT_PROJECT_DIR
        self.local_datasets_dir = os.path.join(self.project_dir, ProjectManager.DEFAULT_LOCAL_DATASETS_DIR)
        self.logging_dir = os.path.join(self.project_dir, ProjectManager.DEFAULT_LOGGING_DIR)
        self.logging_filename = ProjectManager.DEFAULT_LOGGING_FILENAME

        self.has_been_validated = False

    def initialize(self, config_file=None):
        # Load from file
        if config_file is not None:
            assert 'project_dir' in config_file
            self.project_dir = config_file['project_dir']

            for key, value in config_file['directories'].items():
                # Avoid over-writing with None or empty string values
                if hasattr(self, key) and value is not None and len(value.strip()):
                    setattr(self, key, os.path.join(self.project_dir, value))

        self.validate_manager()

    def _validate_folder(self, folder, folder_name, should_create=False):
        if not should_create:
            assert os.path.isdir(folder), f"Expected a directory for {folder_name}, but got {folder}"
        else:
            if not os.path.isdir(folder):
                os.makedirs(folder)

    def validate_manager(self):
        self._validate_folder(folder=self.project_dir, folder_name='project_dir')
        self._validate_folder(folder=self.local_datasets_dir, folder_name='local_datasets_dir', should_create=True)
        self._validate_folder(folder=self.logging_dir, folder_name='logging_dir', should_create=True)

        self.has_been_validated = True

    @classmethod
    def get_manager(cls):
        if cls._instance is None:
            cls._instance = ProjectManager()
        else:
            assert cls._instance.has_been_validated

        return cls._instance
